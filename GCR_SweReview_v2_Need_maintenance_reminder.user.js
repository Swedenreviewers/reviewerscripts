/*

* v01.00					New Version of SweRev-Need maintenance reminder Script. Version 2

// ==UserScript==
// @name					GCR SweReview v2 Need maintenance reminder
// @description				Bookmark and remind caches with false NM-flag.
// @namespace				https://admin.geocaching.com
// @author					Sweden Reviewers Toa Norik
// @version					01.00
// @icon					https://www.geocaching.com/images/attributes/firstaid-yes.gif
// @updateURL				https://gitlab.com/Swedenreviewers/reviewerscripts/raw/master/GCR_SweReview_v2_Need_maintenance_reminder.user.js
// @downloadURL				https://gitlab.com/Swedenreviewers/reviewerscripts/raw/master/GCR_SweReview_v2_Need_maintenance_reminder.user.js
// @include					http*://*.geocaching.com/admin/review.aspx*
// @include					http*://*.geocaching.com/seek/log.aspx*
// @include					http*://*.geocaching.com/bookmarks/mark.aspx*
// @grant					GM_registerMenuCommand
// @grant					GM_getValue
// @grant					GM_setValue
// @grant					GM_openInTab
// @grant					window.close
// ==/UserScript==
*/

// Activate settings menu
	GM_registerMenuCommand('OM-reminder settings', fEditSettings);

//******************* DOM elements and variables ************************//
	// Pre-acquire needed DOM elements.

	// Set variables
	var dftFirstWarn		= "";
	var dftRemWarn			= "";
	var OM_BookMark			= GM_getValue("OM_BookMark", '');
	var FirstWarn			= GM_getValue("OM_FirstWarn", dftFirstWarn);
	var RemWarn				= GM_getValue("OM_RemWarn", dftRemWarn);
	var OM_Auto_Close_Tabs	= GM_getValue("OAN_Auto_Close_Tabs", true);
	var PageUrl				= document.location + "";

	// Images
	var imgsrcOM			=	"data:image/gif;base64,R0lGODlhEAAQAPQAAAAAAJE0N44wM40vMq1dYI" +
								"4vM4UgI4YhJIwtMIknKv///6VaXKJVV6FTVbd7faBRVIwsL/Hn5+jV1ZI2Og" +
								"AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAACH5BAEAABQALA" +
								"AAAAAQABAAAAhqACkEGECwoEGCCQgMPMhwwMKCAw9IPJDAYcMBBxRoVMDg4c" +
								"EAGTcSuEgwpEYGDRkQYMBgowIHKlkijOCypkYJDhPQtOkS5wCdPHsWZMnggU" +
								"uYLB8wNMmx4UKmI0kmcKmUJMaJDKNaJRggIAA7";

// Items in script

//If on cache page
if (PageUrl.match(/https:\/\/.*?\.geocaching\.com\/admin\/review\.aspx/i)) {

//  Get published status for this cache. If not published, exit script.
	var cacheStatus = document.getElementById("ctl00_ContentBody_CacheDetails_Status");
	if (cacheStatus) {
		var cacheStatusText = cacheStatus.firstChild.data;
		if (cacheStatusText.contains('Not Published')) {
			return;
		}
	}

// Check if NM flag is lit. If not NM-flag, exit script.
	var NM_Icon = (document.body.innerHTML.search(/firstaid-yes.\png/g) >= 0);
		if (!NM_Icon)	{
			return;
		}

//  Get cache data element.
	var CacheData = document.getElementById('ctl00_ContentBody_CacheDataControl1_CacheData');
	if (!CacheData) {
		CacheData = document.getElementsByClassName('CacheData')[0];
	}
	if (CacheData) {
//  Get GUID for this cache.
		var CacheGuid = CacheData.getAttribute('data-cacheguid');
	} else {
		GM_log('Unable to locate CacheData element. Script ending.');
		return;
	}

//  Create links.
	//Create warn and bookmark link
	var spanOMWarn = document.createElement('span');
	spanOMWarn.style.marginLeft = '20px';
	spanOMWarn.id = 'spanOMWarn';

	var oakzDiv = document.getElementById("oakzDiv");
	oakzDiv.appendChild(spanOMWarn);

	var lnkOMWarn = document.createElement("A");
	lnkOMWarn.title = 'Bookmark and remind about NM-flag';
	lnkOMWarn.classList.add('OAN_Background');
	lnkOMWarn.href = '/bookmarks/mark.aspx?guid=' + CacheGuid + '&WptTypeID=2&om=y&rem=n';
	spanOMWarn.appendChild(lnkOMWarn);

	var imgOMWarn = document.createElement("IMG");
	imgOMWarn.border = '0';
	imgOMWarn.align = 'absmiddle';
	imgOMWarn.src = imgsrcOM;
	lnkOMWarn.appendChild(imgOMWarn);
	lnkOMWarn.appendChild(document.createTextNode(' Bookmark and remind about NM-flag'));
	lnkOMWarn.addEventListener('click', fWarn_OM_clicked, false);

	//Create reminder link
	var lnkOMRem = document.createElement("A");
	lnkOMRem.id = 'lnkOMRem';
	lnkOMRem.title = 'Remind about NM-flag';
	lnkOMRem.classList.add('KZ_Background');
	lnkOMRem.href = '/seek/log.aspx?wid=' + CacheGuid + '&LogType=68&om=y&rem=y';
	spanOMWarn.appendChild(lnkOMRem);

	var imgOMRem = document.createElement("IMG");
	imgOMRem.border = '0';
	imgOMRem.align = 'absmiddle';
	imgOMRem.src = imgsrcOM;
	lnkOMRem.appendChild(imgOMRem);
	lnkOMRem.appendChild(document.createTextNode(' Remind about NM-flag'));

	} else {
	// If on bookmark page

		//  If not in launched from a OM Warning link, exit.
		if (UrlParm('om') != 'y') { return; }
		//  If log published (check for Edit button), then close window.
		if (PageUrl.search(/seek\/log\.aspx/gi)>=0) {
			if (document.getElementById("ctl00_ContentBody_LogBookPanel1_lnkBtnEdit")) {
				if (OM_Auto_Close_Tabs) {
					window.close();
				}
				return;
			}
		}

		//  Only apply changes if signed on as a reviewer.
		if (document.getElementById("ctl00_ContentBody_LogBookPanel1_chkCannotDelete") != null) {

			//  Get LogType.
			var LogType = UrlParm('LogType');

			//  Function to click the log submit button.
			var ClickLogSubmitButton = function() {
				// xxx var e_LogBookPanel1_LogButton = document.getElementById("ctl00_ContentBody_LogBookPanel1_LogButton");
				var e_LogBookPanel1_LogButton = document.getElementById("ctl00_ContentBody_LogBookPanel1_btnSubmitLog");
				e_LogBookPanel1_LogButton.click();
			}

			//  Get current note text, if any.
			var e_LogBookPanel1_tbLogInfo = document.getElementById("ctl00_ContentBody_LogBookPanel1_uxLogInfo");

			var NoteText = e_LogBookPanel1_tbLogInfo.value;

			//  Default 'Cannot Delete' box to checked.
			var e_chkCannotDelete = document.getElementById("ctl00_ContentBody_LogBookPanel1_chkCannotDelete");
			if (e_chkCannotDelete) {
				e_chkCannotDelete.checked=true;
			}

			// xxx var subButtons = document.getElementsByName("ctl00$ContentBody$LogBookPanel1$LogButton");
			var subButtons = document.getElementsByName("ctl00$ContentBody$LogBookPanel1$btnSubmitLog");
			for (var subButton in subButtons) {
				subButton.disabled = false;
			}


			//  If Review note.
			if (LogType == '68') {
				if (NoteText == "") {
					var wid = UrlParm('wid', true);
					if (UrlParm('rem') == 'y') {
						e_LogBookPanel1_tbLogInfo.focus;
						e_LogBookPanel1_tbLogInfo.value = RemWarn;
					} else {
						e_LogBookPanel1_tbLogInfo.focus;
						e_LogBookPanel1_tbLogInfo.value = FirstWarn;
					}
					//  Start timer function to click Submit button.
					var TimeOutID = window.setTimeout(ClickLogSubmitButton, 250);
				}
			}

		}

		//  If adding to a bookmark, default to the warning bookmark.
		if (PageUrl.search(/bookmarks\/mark\.aspx/gi)>=0) {
			//  If bookmark already submitted, close window.
			if (!document.getElementById("ctl00_ContentBody_Bookmark_btnCreate")) {
				if (OM_Auto_Close_Tabs) {
					window.close();
				}
				return;
			}

			//  Function to click the bookmark submit button.
			var ClickBookmarkSubmitButton = function() {
				var e_Bookmark_btnSubmit = document.getElementById("ctl00_ContentBody_Bookmark_btnCreate");
				if (e_Bookmark_btnSubmit) {
					e_Bookmark_btnSubmit.click();
				}
			};

			var e_BookmarkList = document.getElementById("ctl00_ContentBody_Bookmark_ddBookmarkList");

			if (e_BookmarkList) {
				var e_BookmarkName = document.getElementById("ctl00_ContentBody_Bookmark_tbName");
                // Set bookmark selector. //fix for creating bookmark
                var bmList = document.getElementById('ctl00_ContentBody_Bookmark_ddBookmarkList');
                var ic = bmList.options.length;
                var selId = 0;
                for (var i = 0; i < ic; i++) {
                    if (bmList.options[i].value == OM_BookMark) {
                        selId = bmList.options[i].value;
                        break;
                    }
                }
                if (selId !== 0) {
                    bmList.value = selId;
                    var e_btnSubmit = document.getElementById("ctl00_ContentBody_Bookmark_btnCreate");
                    sessionStorage.setItem('TimedSubmitted', true);
                    e_btnSubmit.click();
                }
			}
		}
	}

// ÄR DETTA ONÖDIGT???? Pausat detta för koll om script funkar
	//if (UrlParm('kzwarn', true) == 'y') {
	//	Warn_clicked();
	//	window.location.replace(lnkWarn.href);
	//}

// End items in script

// General functions

	// Bookmark this cache, in a separate tab.
	function fWarn_OM_clicked() {
		GM_openInTab(curDomain() + '/seek/log.aspx?wid=' + CacheGuid + '&LogType=68&om=y')
	}

	// Returns the current domain, including http or https protocol, without a trailing '/';
	function curDomain() {
		return location.protocol + '//' + document.domain;
	}

	// Remove element and everything below it.
	function removeNode(element) {
		element.parentNode.removeChild(element);
	}

	// Returns a URL parameter.
	// ParmName - Parameter name to look for.
	// IgnoreCase - (optional) *false, true. Ignore parmeter name case.
	// UrlString - (optional) String to search. If omitted, document URL is used.
	function UrlParm(ParmName, IgnoreCase, UrlString) {
		var RegRslt, sc = '', RtnVal = '';
		if (IgnoreCase) {sc = 'i'}
		if(UrlString) {
			var PageUrl = UrlString;
		} else {
			PageUrl = document.location + '';
		}
		var ParmString = PageUrl.substring(PageUrl.indexOf('?') + 1);
		var RegEx1 = new RegExp('(^|&)' + ParmName + '=(.*?)(&|#|$)', sc);
		RegRslt = RegEx1.exec(ParmString);
		if (RegRslt) {RtnVal = RegRslt[2]}
		return RtnVal;
	}

// End general functions

// Settings menu

	// Show Edit Settings box.
	function fEditSettings() {
		// If not on bookmark page.
		var ddBookmarkList = document.getElementById("ctl00_ContentBody_Bookmark_ddBookmarkList");
		if (!ddBookmarkList) {
			alert("You need to be on a bookmark entry page\n" +
					"to edit this script's settings.\n\n" +
					"Use the 'bookmark listing' link from the cache page.");
		return;
		}

		// If div already exists, reposition browser, and show alert.
		var divSet = document.getElementById("gm_divSet");
		if (divSet) {
			var YOffsetVal = parseInt(divSet.getAttribute('YOffsetVal', '0'));
			window.scrollTo(window.pageXOffset, YOffsetVal);
			alert('Edit Setting interface already on screen.');
			return;
		}

		// Create blackout div.
		document.body.setAttribute('style', 'height:100%;');
		var divBlackout = document.createElement('div');
		divBlackout.id = 'divBlackout';
		divBlackout.setAttribute('style', 'z-index: 900; background-color: rgb(0, 0, 0); ' +
				'opacity: 0; position: fixed; left: 0px; top: 0px; '+
				'height: 100%; width: 100%; display: block;');

		// Create div.
		divSet = document.createElement('div');
		divSet.id = 'gm_divSet';
		divSet.setAttribute('style', 'position: absolute; z-index: 1000; ' +
				'padding: 5px; outline: 6px ridge blue; background: #FFFFCC;');
		var popwidth = parseInt(window.innerWidth * .5);
		divSet.style.width = popwidth + 'px';

		// Create heading.
		var ds_Heading = document.createElement('p');
		ds_Heading.style.fontSize = 'medium';
		ds_Heading.style.fontStyle = 'italic';
		ds_Heading.style.fontWeight = 'bold';
		ds_Heading.style.textAlign = 'center';
		ds_Heading.appendChild(document.createTextNode('OM-reminder - Script Settings'));
		divSet.appendChild(ds_Heading);

		// Create first warning text area.
		var ds_FirstwarnTextAreaP = document.createElement("p");
		ds_FirstwarnTextAreaP.style.textAlign = 'left';
		ds_FirstwarnTextAreaP.style.marginLeft = '6px';
		ds_FirstwarnTextAreaP.style.marginRight = '6px';
		ds_FirstwarnTextAreaP.appendChild(document.createTextNode("Enter warning text for Reviewer log:"));
		ds_FirstwarnTextAreaP.appendChild(document.createElement('br'));
		var ds_FirstwarnTextArea = document.createElement("textarea");
		ds_FirstwarnTextArea.id = 'ds_FirstwarnTextArea';
		ds_FirstwarnTextArea.style.width = '100%';
		ds_FirstwarnTextArea.style.height = '160px';
		ds_FirstwarnTextAreaP.appendChild(ds_FirstwarnTextArea);
		divSet.appendChild(ds_FirstwarnTextAreaP);
		ds_FirstwarnTextArea.value = FirstWarn;

		// Create reminder text area.
		var ds_RemTextAreaP = document.createElement("p");
		ds_RemTextAreaP.style.textAlign = 'left';
		ds_RemTextAreaP.style.marginLeft = '6px';
		ds_RemTextAreaP.style.marginRight = '6px';
		ds_RemTextAreaP.appendChild(document.createTextNode("Enter reminder text for Reviewer log:"));
		ds_RemTextAreaP.appendChild(document.createElement('br'));
		var ds_RemTextArea = document.createElement("textarea");
		ds_RemTextArea.id = 'ds_RemTextArea';
		ds_RemTextArea.style.width = '100%';
		ds_RemTextArea.style.height = '160px';
		ds_RemTextAreaP.appendChild(ds_RemTextArea);
		divSet.appendChild(ds_RemTextAreaP);
		ds_RemTextArea.value = RemWarn;

		// Add bookmark selector to page.
		var ds_BookmarkListP = document.createElement("p");
		ds_BookmarkListP.style.textAlign = 'left';
		ds_BookmarkListP.style.marginLeft = '6px';
		ds_BookmarkListP.style.marginRight = '6px';
		ds_BookmarkListP.appendChild(document.createTextNode("Select Bookmark List to use:"));
		ds_BookmarkListP.appendChild(document.createElement('br'));

		var ds_BookmarkList = ddBookmarkList.cloneNode(true);
		ds_BookmarkList.id = "ds_BookmarkList";
		ds_BookmarkList.name = "ds_BookmarkList";
		ds_BookmarkList.setAttribute('style', '');
		ds_BookmarkList.disabled = false;
		ds_BookmarkList.options[0].removeAttribute("selected");
		for (var optie = 0; optie < ds_BookmarkList.options.length; optie++) {
			if (ds_BookmarkList.options[optie].value == OM_BookMark) {
				ds_BookmarkList.options[optie].selected = true;
			}
		}
		ds_BookmarkListP.appendChild(ds_BookmarkList);
		divSet.appendChild(ds_BookmarkListP);

		// Create auto-close checkbox.
		var ds_AutoCloseP = document.createElement("p");
		ds_AutoCloseP.style.textAlign = 'left';
		ds_AutoCloseP.style.marginLeft = '6px';
		ds_AutoCloseP.style.marginRight = '6px';
		var ds_AutoCloseLabel = document.createElement("label");
		ds_AutoCloseLabel.style.fontWeight = 'normal';
		ds_AutoCloseLabel.setAttribute('for', 'ds_AutoCloseCB_OM');
		ds_AutoCloseLabel.appendChild(document.createTextNode("Auto Close Tabs on Completion:"));
		ds_AutoCloseP.appendChild(ds_AutoCloseLabel);

		var ds_AutoCloseCB = document.createElement("input");
		ds_AutoCloseCB.id = "ds_AutoCloseCB";
		ds_AutoCloseCB.name = "ds_AutoCloseCB_OM";
		ds_AutoCloseCB.type = "Checkbox";
		ds_AutoCloseCB.style.marginLeft = '6px';
		ds_AutoCloseCB.checked = OM_Auto_Close_Tabs;
		ds_AutoCloseP.appendChild(ds_AutoCloseCB);

		var ds_AutoCloseInfo = document.createElement("a");
		ds_AutoCloseInfo.style.fontSize = 'x-small';
		ds_AutoCloseInfo.style.marginLeft = '9px';
		ds_AutoCloseInfo.href = 'https://sites.google.com/site/allowscriptstoclosetabs/';
		ds_AutoCloseInfo.title = 'Click to view instructions in a new tab.';
		ds_AutoCloseInfo.target = '_blank';
		ds_AutoCloseInfo.appendChild(document.createTextNode("(Requires configuration change)"));
		ds_AutoCloseP.appendChild(ds_AutoCloseInfo);

		divSet.appendChild(ds_AutoCloseP);


		// Create buttons.
		divSet.appendChild(document.createElement('p'));
		var ds_ButtonsP = document.createElement('p');
		ds_ButtonsP.style.textAlign = 'right';
		var ds_SaveButton = document.createElement('button');
		ds_SaveButton = document.createElement('button');
		ds_SaveButton.appendChild(document.createTextNode("Save"));
		ds_SaveButton.addEventListener("click", fSaveButtonClicked, true);
		var ds_CancelButton = document.createElement('button');
		ds_CancelButton.style.marginLeft = '6px';
		ds_CancelButton.addEventListener("click", fCancelButtonClicked, true);
		ds_CancelButton.appendChild(document.createTextNode("Cancel"));
		ds_ButtonsP.appendChild(ds_SaveButton);
		ds_ButtonsP.appendChild(ds_CancelButton);
		divSet.appendChild(ds_ButtonsP);

		// Add div to page.
		var toppos = parseInt(window.pageYOffset + 60);
		var leftpos = parseInt((window.innerWidth / 2) - (popwidth / 2));
		divSet.style.top = toppos + 'px';
		divSet.style.left = leftpos + 'px';
		divSet.setAttribute('YOffsetVal', window.pageYOffset);

		// Resize/reposition on window resizing.
		function fSetLeftPos() {
			var divSet = document.getElementById('gm_divSet');
			if (divSet) {
				var popwidth = parseInt(window.innerWidth * .5);
				divSet.style.width = popwidth + 'px';
				var leftpos = parseInt((window.innerWidth / 2) - (popwidth / 2));
				divSet.style.left = leftpos + 'px';
			}
		}

		// Add blackout and setting divs.
		document.body.appendChild(divBlackout);
		document.body.appendChild(divSet);
		window.addEventListener('resize', fSetLeftPos, true);
		var op = 0;
		var si = window.setInterval(fShowBlackout, 40);
		// Function to fade-in blackout div.
		function fShowBlackout() {
			op = op + .05;
			divBlackout.style.opacity = op;
			if (op >= .75) {
				window.clearInterval(si);
			}
		}

		// Save values.
		function fSaveButtonClicked() {
			FirstWarn = ds_FirstwarnTextArea.value;
			RemWarn = ds_RemTextArea.value;
			GM_setValue("OM_FirstWarn", FirstWarn)
			GM_setValue("OM_RemWarn", RemWarn)
			OM_BookMark = ds_BookmarkList.options[ds_BookmarkList.selectedIndex].value;
			GM_setValue("OM_BookMark", OM_BookMark);
			OM_Auto_Close_Tabs = ds_AutoCloseCB.checked;
			GM_setValue("OM_Auto_Close_Tabs", OM_Auto_Close_Tabs);
			alert('Settings have been saved.');
			removeNode(divSet);
			removeNode(divBlackout);
			window.removeEventListener('resize', fSetLeftPos, true);
		}

		// Cancel requeste.
		function fCancelButtonClicked() {
			var resp = confirm('Cancel requested. You will lose any changes.\n\n' +
					'Press OK to exit without saving changes. Otherwise, press Cancel.');
			if (resp) {
				removeNode(divSet);
				removeNode(divBlackout);
				window.removeEventListener('resize', fSetLeftPos, true);
			}
		}


	}

// End settings menu