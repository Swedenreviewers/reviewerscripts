/*

* v01.00    New Version of SweRev-Script_FilterRevLogs. Version 2

// ==UserScript==
// @name				GCR SweReview v2 FilterRevLogs
// @description			Filter out logs from archived and active caches
// @namespace		    https://admin.geocaching.com
// @author			    Sweden Reviewers Toa Norik
// @version			    01.00
// @icon			    https://i.imgur.com/zA6Y7hV.png
// @updateURL		    https://gitlab.com/Swedenreviewers/reviewerscripts/raw/master/GCR_SweReview_v2_FilterRevLogs.user.js
// @downloadURL		    https://gitlab.com/Swedenreviewers/reviewerscripts/raw/master/GCR_SweReview_v2_FilterRevLogs.user.js
// @include				http*://www.geocaching.com/my/logs.aspx?s=1&lt=12&cleanup=yes
// @include				http*://www.geocaching.com/my/logs.aspx?s=1&lt=18&cleanup=yes
// @include				http*://www.geocaching.com/my/logs.aspx?s=1&lt=22&cleanup=yes
// ==/UserScript==

*/

// Activate settings menu


//******************* DOM elements and variables ************************//
	// Pre-acquire needed DOM elements.
	// Set variables
	// Images

// Get information

	//Get table
	var mainPane		= $('Content');
						if (!mainPane) { return; }
	var tables			= mainPane.getElementsByClassName('Table');
						if (!tables) { return; }
	var table			=tables[0]
						if (!table) { return; }
	var t, i, row, CacheGUID, LNew, Logdate;
	var Innehall		=''
	var ArcFlag			=0;
	var InactFlag		=0;
	var CacheList		='&';
	var Today			=new Date();


// End getting information

// Items in script
	for (i = 0, row; row = table.rows[i]; i++) {
		Innehall	= row.cells[3].innerHTML;
		Logdate		= row.cells[2].innerHTML.trim();
		var dag=Logdate.substr(8,2);
		if (dag.substr(0,1) == '0') {dag=dag.substr(1,1)}
		var manad=Logdate.substr(5,2);
		if (manad.substr(0,1) == '0') {manad = manad.substr(1,1)}
		LNew = manad + '/' + dag +'/'+Logdate.substr(0,4);
		ArcFlag=Innehall.search("OldWarning") ;
		InactFlag=Innehall.search("Strike") ;
		//Correct to date format
		var cLogdate = Date.parse(Logdate);
		// Count days ago
		var difDays		= Math.floor((Today - cLogdate) / 86400000);
		var NdifDays	= Math.floor((Today - Date.parse(LNew)) / 86400000);
		//alert (cLogdate + ' : ' + Logdate + ' : ' + difDays + ' | ' + LNew + ' : ' + Date.parse(LNew) + ' : ' + NdifDays);
		if (ArcFlag > -1) {
			//Is an arcived cache. Should be hidden
			//Archived=Archived+1;
			row.style.display = 'none';
		} else if (InactFlag == -1) {
			//Is an active cache. Should be hidden
			//Active=Active+1;
			row.style.display = 'none';
		} else {
			//Is an inactive cache. Should be kept but change link to admin page
			//Also check if already visible, hide otherwise
			//Row.cells[3].innerHTML = Innehall.replace(/seek\/cache_details.aspx/g,'admin/review.aspx');
			t=Innehall.search("guid=")
			CacheGUID=Innehall.substr(t+4,36)
			if (CacheList.search('&' + CacheGUID + '&') > -1) {
				//Already visible, hide it
				row.style.display = 'none';
			} else {
				// Not shown before. Keep it and add GUID to list
				CacheList= CacheList + CacheGUID + '&' ;
				if (difDays < 94) {
					//Is an active cache. Should be hidden
					//Active=Active+1;
					row.style.display = 'none';
				}
			}
		}
	}
//End items in script

// General functions
function $() {
	if (arguments.length===1) {
		return document.getElementById(arguments[0]);
	}
	var element, i, len=arguments.length;
	for (i = 0; i < len; i++) {
		var e = arguments[i];
		if (typeof e === 'string') {
			e = document.getElementById(e);
		}
		element.push(e);
	}
	return element;
}
// End general functions

// Settings menu