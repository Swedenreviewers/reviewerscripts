/*
Version history
* v01.03                Removed "require" field

* v01.02                Removed "eval" in line 81. Potentially harmful

* v01.01                Cleanup in code

* v01.00				Initial release.

// ==UserScript==
// @name				GCR SweReview v2 Filter time set caches
// @description			Filter out caches that are set for time publish
// @namespace			https://admin.geocaching.com
// @author				Sweden Reviewers Toa Norik
// @version				01.03
// @icon				https://i.imgur.com/zA6Y7hV.png
// @updateURL			https://gitlab.com/Swedenreviewers/reviewerscripts/raw/master/GCR_SweReview_v2_Filter_time_set_caches.user.js
// @downloadURL			https://gitlab.com/Swedenreviewers/reviewerscripts/raw/master/GCR_SweReview_v2_Filter_time_set_caches.user.js
// @include				http*://*.geocaching.com/admin/queue.aspx*
// @grant				GM_getValue
// @grant				GM_setValue
// @grant				GM_addStyle
// ==/UserScript==
*/

//******************* DOM elements and variables ************************//
	// Pre-acquire needed DOM elements.
	var e_DrpList		= $('ctl00_ContentBody_HyperLink_Permalink').parentNode;

	// Styles

	// Create class for 50% opaque.
	GM_addStyle(".gms_faded { opacity:0.35; }");


	// Images
	var LockIcon	=	"data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAABAAAAAQCAYAAAAf8/" +
						"9hAAAABmJLR0QA/wD/AP+gvaeTAAAACXBIWXMAAAsTAAALEwEAmpwYAAAAB3RJTU" +
						"UH3gUUCwgdVPGRdwAAAqhJREFUOMttks1rVGcUh5/3vXPHyUc7xCwmSo1pcGGTSj" +
						"alYkSxFaG7QhEX0U0NVKTdtH9A66IUSjGEQhEXUrppQXCluBBJF4IgBJkmaU2rMY" +
						"011ThJ5k6Tm7n3vh+ni8mEhOS3Oi8v5zm/86FEBIBLl64BEASaIAhaWloKH+3e/d" +
						"oJpQI9NTVxf2zs5o1y+X7knKUpESHXfCilALDWFYrF9m8GBw99sn9/VysIBw/uOx" +
						"eG4ZGZmUefR1FlhU3aACRJinOOjo7iyJkz71/s6upkbu4lWZYxMPBmvrf34+EoWt" +
						"515crX50W8aebpZlCv14njtXeOHn37QqnUwdWr1xeHhoZHzp49/+Xlyz/9o5RhaO" +
						"j0uZ6e/lM7OojXDK0tuff6+3v0+PgjRkdHvp+efvAtkC0uVR8cONB96/jxd8NSae" +
						"/J2dnJ29sc2KRGnPj2MAx49uyFqyy+eghk7QXN7NOJe389no0Fj6CKOzq40HeTO/" +
						"OD8mqhRn11FTFGD/bmGB3u5t4fSbhqUlWr1lAmlc0A1Vzj76NvfUGp79P27oHelS" +
						"iSaOFx+Y0OX8mHGu9VuCTdx4qdnbmXTyb//XVs/Ievfn7+XWa92QBM/vJheuj0nj" +
						"x2AVQAuTx4BdIsaAABNOUfZ/yRz8r9deumN1qoxfWElYk8WQIotkkr8AJPK1T+9p" +
						"kV3bplBsuLRqhmkGYNwGaGUmAczNdgOaZaaxPrka2HFDuo1iFNGglCw74AqW38rW" +
						"WAJ0399i0o42A1BW/BSqOicWB8A2A8WAfeoaxsBwSBzlHQEFtwHpxrJJj12K0DvS" +
						"UQHzQHtQGYe54u/TnV1lYwCWI1OA02AOsbjqxDOU9qFE+WCytai9lyB31drx9u25" +
						"X/QCnR6/vaSQqUfvFf8tt8Nb7rRaL/ATCDW8mGcPIzAAAAAElFTkSuQmCC";

// Get information

	// Get currently signed-on geocaching.com profile.
		var SignedInAs = document.getElementById("ctl00_LoginUrl");
		if (!SignedInAs) { SignedInAs = document.getElementById('ctl00_LogoutUrl'); }
		SignedInAs = SignedInAs.parentNode.childNodes[1].firstChild.data.trim();
		SignedInAs = SignedInAs.replace(/<\&amp;>/g, '&');

// End getting information

// Items in script

	// Add locked image.
	var imgTP = document.createElement("img");
	imgTP.src = LockIcon;
	imgTP.id = 'imgEC';
	imgTP.border = '0';
	imgTP.style.marginLeft = '14px';
	imgTP.style.marginRight = '4px';
	imgTP.style.verticalAlign = 'text-bottom';
	e_DrpList.parentNode.appendChild(imgTP);

	// Create checkbox.
	var cbTP = document.createElement("input");
	cbTP.type = 'checkbox';
	cbTP.id = 'cbTP';
	cbTP.style.marginRight = '8px';
	cbTP.title = 'Uncheck to filter out caches that are set to Time Publish';
	cbTP.checked = GM_getValue("FilterLockedcaches", false);
	if (cbTP.checked == true) {
		imgTP.classList.add('gms_faded');
	}
	insertAfter(cbTP, imgTP);
	cbTP.addEventListener("click", fLockFilterClicked, true);
	var bLock = cbTP.checked;

	// Get table
	var mainPane = $('bd');
	if (!mainPane) { return; }
	var tables = mainPane.getElementsByClassName('Table');
	if (!tables) { return; }
	var table=tables[0]
	if (!table) { return; }

	for (var i = 0, row; row = table.rows[i]; i++) {
		var Innehall=row.cells[5].innerHTML;
		var els = document.getElementsByClassName("time-publish-status");
		if (els) {
			var Antal = 0;
			for (var x = 0; x < els.length; x++) {
				if (els[x].innerHTML.indexOf("Set to publish") != -1) {
					Antal++
					if (cbTP.checked) {
						els[x].parentNode.parentNode.style.visibility = 'collapse';
					} else {
						els[x].parentNode.parentNode.style.visibility = 'visible';
					}
				}
			}
		}
	}

// End items in script


// General functions

// Locked caches Filter Checkbox Changed.
function fLockFilterClicked() {
	GM_setValue("FilterLockedcaches", cbTP.checked);
	imgTP.classList.toggle('gms_faded');
	FHideLockedCaches();
}

// Step through and hide locked caches
function FHideLockedCaches() {
	var els = document.getElementsByClassName("time-publish-status");
	if (els) {
		var Antal = 0;
		for (var x = 0; x < els.length; x++) {
			if (els[x].innerHTML.indexOf("Set to publish") != -1) {
				Antal++
				if (cbTP.checked) {
					els[x].parentNode.parentNode.style.visibility = 'collapse';
				} else {
					els[x].parentNode.parentNode.style.visibility = 'visible';
				}
			}
		}
		//Set title
		if (cbTP.checked) {
			cbTP.title = 'Unheck to show ' + Antal + ' timeset caches.';
		} else {
			cbTP.title = 'Check to hide ' + Antal + ' timeset caches.';;
		}
	}
}

// Insert element after an existing element.
function insertAfter(newElement, anchorElement) {
	anchorElement.parentNode.insertBefore(newElement, anchorElement.nextSibling);
}

// Function for getting elements by ID
function $() {
	if (arguments.length===1) {
		return document.getElementById(arguments[0]);
	}
	var element, i, len=arguments.length;
	for (i = 0; i < len; i++) {
		var e = arguments[i];
		if (typeof e === 'string') {
			e = document.getElementById(e);
		}
		element.push(e);
	}
	return element;
}

// End general functions