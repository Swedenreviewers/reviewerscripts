// ==UserScript==
// @name Fornsök
// @author olalb
// @version  1
// @include https://app.raa.se/open/fornsok/plats-query*
// @license The MIT License (MIT)
// @run-at document-idle
// ==/UserScript==
function getUrlVars() {
    var vars = [], hash;
    var hashes = window.location.href.slice(window.location.href.indexOf('?') + 1).split('&');
    for (var i = 0; i < hashes.length; i++) {
        hash = hashes[i].split('=');
        vars.push(hash[0]);
        vars[hash[0]] = hash[1];
    }
    return vars;
}

function init() {
    let stillLoadig = document.getElementsByClassName('loading');
    if (stillLoadig.length > 0) {
        //If map hasn't loaded, wait 200ms and try again
        setTimeout(init, 200);
    }
    else {
        let vars = getUrlVars();
        /* Only do something if both N and E are present */
        if (typeof(vars['n']) !== "undefined" && typeof(vars['e']) !== "undefined") {
            /* Switch to orto-map */
            /* We need to "click" map view button first for it it create the selection */
            let mapButton = document.getElementsByClassName('raa-icon-map')[0];
            mapButton.click();
            let ortoButton;
            let iter = 0;
            let mapBreak = false;
            do {
                ortoButton = document.getElementsByClassName("sprite-ortofoto");
                iter++;
                if (iter > 100) {
                    mapBreak = true;
                    break;
                }
            }
            while(typeof(ortoButton) === "undefined");
            if (!mapBreak) {
                ortoButton[0].parentElement.click();
            }

            /* Set E and N */
            let nInput = document.getElementById('koordinat_n_input');
            let eInput = document.getElementById('koordinat_e');
            nInput.value = vars['n'];
            eInput.value = vars['e'];
            /* Fornsök uses angular, so we need to trigger the correct event */
            nInput.dispatchEvent(new Event('input'));
            eInput.dispatchEvent(new Event('input'));

            let searchButton = document.getElementsByClassName('raa-button-confirm');
            searchButton[0].click();
        }
    }
}

// window.onload = function() {
//     alert('window.onload');
    init();
// };
// console.log('script started');
