/*
* v01.22    Added check for wrongly set challenge attribute

* v01.21    Test of update function

* v01.20    Added check for challenge attribute

* v01.10    Changed name on script

* v01.03    Clean up in code
            Verified that update works

* v01.02    Fixed update link

* v01.01    Minor adjustments in code
            Testing of update function

* v01.00    New Version of SweRev-Script. Version 2

// ==UserScript==
// @name			    GCR SweReview v2 Review page enhancer
// @description		    Enhancements to the GC Reviewer page for Swedish Reviewers
// @namespace		    https://admin.geocaching.com
// @author			    Sweden Reviewers Toa Norik
// @version			    01.22
// @icon			    https://i.imgur.com/zA6Y7hV.png
// @updateURL		    https://gitlab.com/Swedenreviewers/reviewerscripts/raw/master/GCR_SweReview_v2_Review_page_enhancer.user.js
// @downloadURL		    https://gitlab.com/Swedenreviewers/reviewerscripts/raw/master/GCR_SweReview_v2_Review_page_enhancer.user.js
// @include			    http*://*.geocaching.com/admin/review.aspx*
// @require			    SweRevVerChk.js
// @grant			    GM_getValue
// @grant			    GM_setValue
// @grant			    GM_addStyle
// @grant			    GM_xmlhttpRequest
// @grant			    GM_openInTab
// @grant			    GM_registerMenuCommand
// @grant			    window.close
// ==/UserScript==
*/

// Activate settings menu
GM_registerMenuCommand('Edit "GCR SweReview v2" script settings', fEditSettings);

//******************* DOM elements and variables ************************//
	// Pre-acquire needed DOM elements.
	var e_CacheDetails_Coords	= $("ctl00_ContentBody_CacheDetails_Coords");
	var e_waypointTable			= $('ctl00_ContentBody_Waypoints');
	var e_lastupdate			= $('ctl00_ContentBody_CacheDetails_DateUpdated');
	var e_pagegenerated			= $('ctl00_ContentBody_CacheDetails_PostDate');
	var e_UserInfo_lnkfinds		= $('ctl00_ContentBody_UserInfo_lnkFinds');
	var e_CacheDetails_Status	= $('ctl00_ContentBody_CacheDetails_Status');
	var e_cacheData				= $('ctl00_ContentBody_CacheDataControl1_CacheData');
	var e_UserInfo_lnkHides		= $('ctl00_ContentBody_UserInfo_lnkHides');
	var e_ZoneGM				= $('ZoneGM');

	// Set variables
	var Today					= new Date();
	var Logdate					= e_lastupdate.innerHTML;
		Logdate					= Logdate.substring(0, 19);
	var cLogdate				= Date.parse(Logdate);
	var reffounds				= 0;
	var PhysAtStart				= 0;
	var virfounds				= 0;
	var finalWpFound			= 0;
	var erad					= 0;
	var bPublished				= false;
	var bDisabled				= false;
	var bArchived				= false;
	var finalhidden				= 'dold';
	var numWaypoints			= e_cacheData.getAttribute('data-wp_count');
	var CoordDdLat				= e_cacheData.getAttribute('data-latitude') - 0;
	var CoordDdLon				= e_cacheData.getAttribute('data-longitude') - 0;
	var Caw						= GM_getValue("Caw", false);
	var CcaR					= GM_getValue("CcaR", false);
	var Ctsle					= GM_getValue("Ctsle", false);
	var Cina					= GM_getValue("Cina", false);
	var Cffh					= GM_getValue("Cffh", false);
	var waypoints				= [];
	var raawaypoints			= [];
	var addWP					= [];
	var distwp					= [];

	// Images
	var GeneralWarningImg =
		"data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAABgAAAAYCAYAAADgdz34AAAE" +
		"EElEQVR42s2Va2wUVRTH%2F3dmOn1tt8u23b63BW0MQmIgQVJEVOSRSHdJSkRitFb5QqLx" +
		"izF8UuojxugXY2JQE8MXXx%2BA2lbUNhIxKG0CJK3YYEu3S9dut%2Fvqe7vdeV3PTMmSZc" +
		"VW1MRJbu7dc885v7nnP%2Fcsw3%2F8sP8VYKip%2BagOXnvv1%2B0v%2FOuAod1eXrRxLT" +
		"gTEPvVh%2Ft6OlcVuyqna3u8vMLzAGRRAY9FkVIYfBcD2NzdvmL8ig7XvQePiY6iNte2dV" +
		"BHRihChCSLCIzMIrGotG3qOv3aPwL4HmniNUe8SP3YA1ZSSRYOHpmEZHfiyi8RbO3%2Bit" +
		"0xILDvQKRwy%2FqyPGEKOQ%2FtR86OPZZdOdcD7dQJTCVyEAovhO%2F%2Fpr3izgB7m3lF" +
		"6y6kzn0H24cdGXtzLR7kllfhylAcW7pOsb8NCO7Zz%2B3e7ZASIWh%2BP2zHT2cCnn4MUr" +
		"kbc7MJ%2BIPz2PZtB1s1YNJ78HVWUvSK89ENUHx%2BGKHxbECrF5KrFjJpMuifRlLV3mjs" +
		"OPnqqgChnU3c%2FtRuWoxaX40RmoDt%2FS8yfGYe3gihqh6CqkKXCnAppmLX92fYioCw9%" +
		"2FFOuaHGk7%2FJDT4ehB6PQbvci%2BKzv2X4xdcWQKy9G1B1yDpH%2F%2FUYwkne1bwQ8%" +
		"2F4lIHKghTsONSLZ3QFjegqCKMMYHYHjYigT0GBPA5gGyAZD1%2BA4nkjE2W0Bkzv2TuQ2" +
		"lFfKYhjaYmp52%2BAwfMNw9AVvC%2BAqh6QDv8cXMBCdDTy5MFWXBQi76jzaktrpfKcF6s" +
		"994JJkBVsA%2FygcF8YyAeudECvr0wCmGigivU4OB82s3kNz8a4MQCivlNvbnoXu66cguq" +
		"2mUbsBmJlGzs4mFL70puW78PZRpL48AdFZS76aBQABRCpTMqHgzHgEh5MzLA2IuuqOweVs" +
		"K3imEfrVYbII4IaxDNBpVlTqQ1dhTE4vH9teSKMYor0iA8A1jiLOcHYihmhy6aOWxdkjFm" +
		"BScHD7Jy9DO%2F8DIEpW0jTAPEEkBNt7n0PasNkCaAOXMPdcM4Ti0nSJTAAIINBsiv5pII" +
		"jnU%2FOMRddU98meB7cKbomaWJx6Gc8G%2BK6RyOOZGtzjgFi9LgtgzrkUNjgzj%2F75uS" +
		"iL5pVx2%2FEXoVw4T3dKpuRaNmCMRP7pVpFLSWT3nwLMkU%2Bl%2BmxiAixaVs9t77ZC6e" +
		"0lQczy6FkAJBIAtefCtz6AUFJBjW4ftNEhiI6qtAb8FoCTcn0cDBDAUT2W59nuZncJwHzy" +
		"JkAzLxANWjMq29LAIAUq5t8BWH4%2BaSUi11EOpJZPICg3ATIV4XJ8xizR8lcUXlMzRjnc" +
		"cNlg0LU36O11xojDodMGpbB%2Bm4fRuW7NKtkNgms39jVaW%2F4wEF1SyG4EDi%2FO1v0B" +
		"daNJ3oB8YjwAAAAASUVORK5CYII%3D";


// Get information
	// Get cache status
	if (e_CacheDetails_Status) {
		bArchived = (e_CacheDetails_Status.firstChild.data.match('Archived') != null);
		bDisabled = (e_CacheDetails_Status.firstChild.data.match('Inactive') != null);
		bPublished = (!e_CacheDetails_Status.firstChild.data.match('Not Published'));
	} else {
		bPublished = true;
	}

	// Get coordinates for this cache.
	var txCoords = e_CacheDetails_Coords.firstChild;
	while (txCoords.nodeName != '#text') {
			txCoords = txCoords.firstChild;
	}
	txCoords = txCoords.data;

// End getting information

// Items in script

// Check time since last edit
	if (Ctsle) {
		if (!bPublished && !bDisabled) {
			var Opendate = e_pagegenerated.innerHTML;
			Opendate = Opendate.substring(0, 19);
			var cOpendate = Date.parse(Opendate);
			var difTime = cOpendate - cLogdate;
			var difDay = Math.floor(difTime / 86400000);
			difTime = difTime - (difDay * 86400000);
			var difHrs = Math.floor(difTime / 3600000);
			difTime = difTime - (difHrs * 3600000);
			var difMin = Math.floor(difTime / 60000);

			//Add text about time since edit
			if (difDay >= 2) {
				fShowAlert("Edited " + difDay + " days " + difHrs + " hours and " + difMin + " min ago ");
			}
		}
	}

// Check and analyze all aditional waypoints
	if (Caw == true || CcaR == true) {
		// Get cache info
		waypoints.push({'icon'			: 'http://www.geocaching.com/images/wpttypes/sm/' + e_cacheData.getAttribute('data-cachetypeid') + '.gif',
						'wpid'			: e_cacheData.getAttribute('data-cachetypeid')
					});

		// Find waypoints
		if (e_waypointTable && (e_waypointTable.nodeName === 'TABLE')) {

			// Loop through waypoints and extract info
			for (i=0; i < numWaypoints; i++) {
				var wRow = e_waypointTable.rows[i*2+1];
				var wImg = wRow.cells[1].firstElementChild;
				if (wImg.nodeName !== 'IMG') {
					wImg = wImg.firstElementChild;	// Review_Page script wraps the IMG in an A tag
				}
				waypoints.push({'icon'			: wImg.src,
								'wpid'			: e_cacheData.getAttribute('data-wp_' + i + '_type')
								});
				if ((waypoints[i+1].wpid) == '220' ) {
					if ((waypoints[i+1].icon) == 'https://www.geocaching.com/images/icons/icon_notviewable.jpg'){
						finalhidden = 'dold';
					} else if ((waypoints[i+1].icon) == 'https://www.geocaching.com/images/icons/icon_nocoords.jpg'){
						finalhidden = 'semidold';
					} else {
						finalhidden = 'synlig';
					}
				}
			}
		}
	}

// Check additional waypoints
	if (Caw) {
		// If Puzzle or Multi, and no Final waypoint, display error.
		if (!bPublished)	{
			// establish what type of cache it is. 2 = trad, 3 = Multi, 5 = letterbox, 8 = mystery, 1858 = WherIgo
			var cd_cachetypeid = e_cacheData.getAttribute('data-cachetypeid');
			// Check if posted coordinate are physical or virtual or N/A.
			var PostedCoordType = $('ctl00_ContentBody_CacheDetails_pcPhysical');
			if (!PostedCoordType.checked) {
				PostedCoordType = $('ctl00_ContentBody_CacheDetails_pcVirtual');
				if (!PostedCoordType.checked) {
					PostedCoordType = "N/A";
				} else {
					PostedCoordType = "Virtual";
				}
			} else {
				PostedCoordType = "Physical";
			}

		// Check if cache is mystery, multi letterbox or WIG. Give warnings of additional waypoints
			if (cd_cachetypeid == '3' || cd_cachetypeid =='5' || cd_cachetypeid =='8' || cd_cachetypeid =='1858') {
				var cd_wp_count = e_cacheData.getAttribute('data-wp_count') - 0;
				for (var i = 0; i < cd_wp_count; i++) {
					var wpType = e_cacheData.getAttribute('data-wp_' + i.toString() + '_type');

					if (wpType == '452') {
						// Found reference point record coordinates
						var finalLat = e_cacheData.getAttribute('data-wp_' + i.toString() + '_latitude') - 0;
						var finalLon = e_cacheData.getAttribute('data-wp_' + i.toString() + '_longitude') - 0;
						// Increase counter
						reffounds++;
					} else if (wpType == '219' || wpType == '220'){
						// Found SoaM or Final stage. Check if placed at start
						if (fDistCalc(CoordDdLat, CoordDdLon, e_cacheData.getAttribute('data-wp_' + i.toString() + '_latitude') - 0, e_cacheData.getAttribute('data-wp_' + i.toString() + '_longitude') - 0, 'mi') < 0.0001) {
							// Placed at start
							PhysAtStart++;
						}
					} else if (wpType == '218'){
						// Found Virtual. Check if placed at start
						if (fDistCalc(CoordDdLat, CoordDdLon, e_cacheData.getAttribute('data-wp_' + i.toString() + '_latitude') - 0, e_cacheData.getAttribute('data-wp_' + i.toString() + '_longitude') - 0, 'mi') < 0.0001) {
							// Placed at start
							virfounds++;
						}
					}
				}

				if (!reffounds && !PhysAtStart && PostedCoordType == "Physical") {
					// Found no reference point and no Physical stage but stage is physical.
					fShowAlert('No waypoint entered for posted coordinate though it is physical');
				}

				if (PhysAtStart && PostedCoordType == "Virtual") {
					// Found unnecessary reference point.
					fShowAlert('Physical stage added at posted coordinates though it is virtual ');
				}

				if (reffounds && cd_cachetypeid == "8" && PostedCoordType == "Virtual") {
					// Found unnecessary reference point.
					fShowAlert('Unnecessary reference point added');
				}

				if (PhysAtStart > 1) {
					// Found more than one physical waypoint at posted coordinates
					fShowAlert('Multiple Physical Waypoints at Start');
				}

				if (virfounds && PostedCoordType == "Physical") {
					// Found more than one virtual waypoint at posted coordinates event though it is physical
					fShowAlert('Virtual stage added at posted coordinate though it is physical');
				}

				if (finalhidden == 'semidold') {
					// Found more than one virtual waypoint at posted coordinates event though it is physical
					fShowAlert('Final waypoint is visible with hidden coordinates');
				}

				if (finalhidden == 'synlig') {
					// Found more than one virtual waypoint at posted coordinates event though it is physical
					fShowAlert('Final waypoint is not hidden');
				}
			}
		}
	}

// Check cache against RAA
	if (CcaR) {
  /*
		// Trim coords for RAA test
		var str=txCoords;
		str=str.replace(/ /g,"%20");
		str=str.replace(/°/g,"%C2%B0");

		// Find waypoints coordinates
		var n = 0
		if (e_waypointTable && (e_waypointTable.nodeName === 'TABLE')) {
			// Loop through waypoints and extract info
			for (i=0; i < numWaypoints; i++) {
				wRow = e_waypointTable.rows[i*2+1];
					distwp.push({'dist'			: wRow.cells[6].lastElementChild.textContent});
						var orgString		= distwp[i].dist;
						var MileVal			= distwp[i].dist;

				var fyswp = wRow.cells[5].lastChild.textContent.trim().slice(1,-1)
				if (fyswp == 'Final Location' || fyswp == 'Physical Stage') {
					raawaypoints.push({'name'			: wRow.cells[5].firstElementChild.textContent,
										'row'			: i,
										'coords'		: { 'lat'	: e_cacheData.getAttribute('data-wp_' + i + '_latitude'),
															'lon'	: e_cacheData.getAttribute('data-wp_' + i + '_longitude') }
										});
					DDddddd_to_DDMMmmm(raawaypoints[n].coords.lat, raawaypoints[n].coords.lon)
					//Trim coords for RAA test
					var tmpcoords=tmpcoords.replace(/ /g,"%20");
					tmpcoords=tmpcoords.replace(/°/g,"%C2%B0");
					str = str + '|' + tmpcoords ;
					addWP.push(tmpcoords);
					n++;
				}
			}
		}

		// add some styles to the page
		LD_addStyle('img.link			{ cursor:pointer; }' +
					'span.link			{ color:#00538E; cursor:pointer; }' +
					'span.link:hover	{ color:#990000; }' +
					'span.link span		{ text-decoration:underline; }' +
					'span.link img		{ margin-left:6px; vertical-align:text-bottom; }' +
					'#CacheDetails_Name, #CacheDetails_Status, ' +
					'#CacheDetails_Coords, #scrollToWaypoints { white-space:nowrap; }');

		// Define some icons and colours
		var ikonGrund = 'http://www.geocaching.com/images/silk/';
		var ikonRaaKoll = ikonGrund + 'hourglass.png';
		var ikonArbetar	= ikonGrund + 'hourglass.png';		var colourWorking	= '#A9A9A9';	// gray
		var ikonKlart	= ikonGrund + 'accept.png';			var colourAllClear	= '#C6E3C0';	// pale green
		var ikonTraff	= ikonGrund + 'error.png';			var colourSomeZone	= '#DDA0DD';	// plum
		var ikonError	= ikonGrund + 'cross.png';			var colourProblem	= '#FF2222';	// bright red
		var ikonWarning	= ikonGrund + 'cross.png';			var colourWarning	= '#FFFF88';	// pale yellow

		Create_ZoneCatcher_Results_Panel();
		Create_ZoneCatcher_Map_Link();
		Check_Zone_Status();

		// Functions for RAA

		function LD_addStyle(css, theID) {
		var styleSheet = document.createElement('style');
		styleSheet.type = 'text/css';
		styleSheet.appendChild(document.createTextNode(css));
		if (theID) {
			LD_removeNode(theID);	// no duplicate IDs
			styleSheet.id = theID;
		}
		document.getElementsByTagName('head')[0].appendChild(styleSheet);
		}

		function DDddddd_to_DDMMmmm(inilat,inilon){
		//Convert to number
		inilat		= parseFloat(inilat);
		// Degrees
		var grader		= Math.floor(inilat);
		// Minutes
		var minuter		= ((inilat-grader)*60).toFixed(3);
		if (minuter < 10) { minuter = '0' + minuter;}
		tmpcoords = 'N ' + grader + '° ' + minuter;
		//Convert to number
		inilon		= parseFloat(inilon);
		// Degrees
		grader		= Math.floor(inilon);
		if (grader<100) { grader = '0' + grader;}
		// Minutes
		minuter		= ((inilon-grader)*60).toFixed(3);
		if (minuter < 10) { minuter = '0' + minuter;}
		tmpcoords = tmpcoords + ' E ' + grader + '° ' + minuter;
		}

		function Create_ZoneCatcher_Results_Panel() {
			var html = '<table width="100%" cellspacing="0"><tbody>';
				html +=		'<tr id="raaHeaderRow" style="background-color:' + colourAllClear + ';">' +
							'<td style="padding:2px 3px 2px 10px; border-bottom:1px solid #35598E;' +
							' background-color:inherit; white-space:nowrap;">' +
							'<span id="zoneOptions">' +
							'</td></tr>' +
							'<tr id="zoneDisplayRow" style="display:none; background-color:#FFFFFF;">' +
							'<td colspan="2" style="border-bottom:1px solid #35598E;">' +
							'<div id="raaDiv" style="height:' +
							'em; overflow:auto;"></div></td></tr>';

			var newDiv = document.createElement('div');
			newDiv.innerHTML = html;
			insertAfter(newDiv, e_ZoneGM);
		}

		function Create_ZoneCatcher_Map_Link() {
		var dtRaaIkon = newElement('li', {},
			newElement('img',		{id		: 'raaIkon',
									src		: ikonRaaKoll,
									'class'	: 'icon',
									title	: 'Uppdatera Riksantikvarie\u00e4mbetet',
									onclick	: Check_Zone_Status } ),
			newElement('a',			{id		:'RaaLink',
									'class' : 'btn-link',
									title	: 'Oppna Riksantikvarie\u00e4mbetets karta i nytt fonster',
									onclick	: open_raa},
			newElement('Riksantikvarie\u00e4mbetets karta')
			),
		);

		var lnkLitmus = $('ctl00_ContentBody_lnkCacheHistory');
		lnkLitmus.parentNode.parentNode.appendChild(dtRaaIkon);
}

		function Check_Zone_Status() {
		var numberofhits = 0;
		var numberofhitsaddwp = 0;
		var raaHeader	= $('raaHeaderRow');
		var raaDiv		= $('raaDiv');
		var raaIkon		= $('raaIkon');
		raaIkon.src		= ikonArbetar;
		raaIkon.title	= raaDiv.innerHTML	= 'Getting data . . .';
		raaHeader.style.backgroundColor = colourWorking;
		GM_xmlhttpRequest( {
			method: 'GET',
			//url: checkURL,
			url: "http://www.swedenreviewers.se/raa/test.asp?latlon=" + str + "&Zoom=200&go=go&emergency=1",
			onload: function(result) {
				var zoneData = result.responseText;
				if (zoneData.substr(0,3) == 'HIT' || zoneData.substr(0,5) == 'NOHIT' ) {
					var n = zoneData.length;
					var f = zoneData.indexOf(",");
					if (f > 0 ) {
						var mainWP = zoneData.substr(0, f)
							if (mainWP == 'HIT') { numberofhits++; }
								zoneData = zoneData.substr(f+1, n);
								zoneData = zoneData.split(",");
								for (i=0; i < raawaypoints.length; i++) {
								raawaypoints[i].raa = zoneData[i];
								if (raawaypoints[i].raa == 'HIT') {
									numberofhits++;
									numberofhitsaddwp++;
								}
							}
					} else {
					mainWP = zoneData;
					if (mainWP == 'HIT') { numberofhits++; }
					}
				} else {
				numberofhits = "ERR";
				}

				// no zones? this is good.
				if (numberofhits == 0) {
					raaIkon.src = ikonKlart;
					raaIkon.title = 'Inga waypoints ligger vid ett fornminne.';
					raaHeader.style.backgroundColor = colourAllClear;
					raaDiv.innerHTML = raaIkon.title;
				} else {
						if (numberofhits > 0) {
							raaIkon.src = ikonTraff;
							raaIkon.title = 'Minst en waypoint ligger nära ett fornminne.';
							raaHeader.style.backgroundColor = colourWarning;
							raaDiv.innerHTML = raaIkon.title;
						} else {
								raaIkon.src = ikonError;
								raaIkon.title = 'RAA svarade inte.';
								raaHeader.style.backgroundColor = colourProblem;
								raaDiv.innerHTML = raaIkon.title;
								}
						}

				for (var w = 0; w < waypoints.length; w++) {
					if (zoneData[w]== "HIT") {
						n = raawaypoints[w].row;
						e_waypointTable.rows[n*2+2].cells[0].style.backgroundColor = colourWarning;
						var AddWPRAAImg = document.createElement('img');
						AddWPRAAImg.src = ikonTraff;
						var AddWPRAALnk = document.createElement('a');
						AddWPRAALnk.name = 'RAA-WP ' + n;
						AddWPRAALnk.id = 'RAA-WP ' + n;
						AddWPRAALnk.title = 'Show this coordinates at Riksantikvarieambetet';
						AddWPRAALnk.href = 'http://www.swedenreviewers.se/raa/konv1.asp?latlon='
							+ addWP[w] + '&Zoom=200&go=gorev&emergency=1';
						AddWPRAALnk.target = "_blank"
						e_waypointTable.rows[n*2+2].cells[0].appendChild(AddWPRAALnk);
						AddWPRAALnk.appendChild(AddWPRAAImg);
					} else {
						n = raawaypoints[w].row;
						e_waypointTable.rows[n*2+2].cells[0].style.backgroundColor=colourAllClear;
						AddWPRAAImg = document.createElement('img');
						AddWPRAAImg.src = ikonKlart;
						AddWPRAALnk = document.createElement('a');
						AddWPRAALnk.name='RAA-WP ' + n;
						AddWPRAALnk.id = 'RAA-WP ' +n;
						AddWPRAALnk.title = 'Denna waypoint ligger inte vid ett fornminne'
						AddWPRAALnk.href = 'http://www.swedenreviewers.se/raa/konv1.asp?latlon='
							+ addWP[w] + '&Zoom=200&go=gorev&emergency=1';
						AddWPRAALnk.target ="_blank"
						e_waypointTable.rows[n*2+2].cells[0].appendChild(AddWPRAALnk);
						AddWPRAALnk.appendChild(AddWPRAAImg);
					}
				}
			}
		}
		);
	}
    */
	}


// Check for first hide
	if (Cffh) {
		// Insert Hides Warning.
		var HidesCount = e_UserInfo_lnkHides.innerHTML + "";
		HidesCount = parseInt(HidesCount.replace(/[^0-9]/g,""));
		if (HidesCount == 0) {
			fShowAlert('First hidden cache');
		}
	}

// Check if need archive
	if (Cina) {
		if (!bArchived && !bPublished) {
			var difDays = Math.floor((Today - cLogdate) / 86400000);
			// Add text about archive if older than 90 days
			if (difDays >= 90) {
				fShowAlert('Archive this due to inactivity.');
			} else {
				// Add text how long time left to archive if older than 76 days
				if (difDays > 76){
					fShowAlert ((90 - difDays) + ' day(s) left to archive due to inactivity.');
				}
			}
		}

	// Add text how long time since edit between 4 and 9999 days ago and cache is active but not published
		if (difDays < 9999 && difDays >= 4) {
			fShowAlert ((difDays) + ' days since last edit/log.');
			} else {
			// Add text how long time since edit and cache is inactive but not published
			if (bDisabled && difDays < 9999){
				fShowAlert ((difDays) + ' days since last edit/log.');
			}
		}
	}

//TEST FOR CHALLENGE ATTRIBUTE

var PathSearch = "//a[contains(@href, 'project-gc.com/Challenges/')]";
var ccLink = document.evaluate(PathSearch, document, null,
                               XPathResult.FIRST_ORDERED_NODE_TYPE, null).singleNodeValue;
if (ccLink) {

// Check if Challenge attribute is lit.
	var C_Icon = (document.body.innerHTML.search(/challengecache-yes.\png/g) >= 0);
		if (C_Icon)	{
			return;
		}
    fShowAlert('No attribute for challenges');
} else {
    // Check if Challenge attribute is lit.
	C_Icon = (document.body.innerHTML.search(/challengecache-yes.\png/g) >= 0);
		if (!C_Icon)	{
			return;
		}

    fShowAlert('Attribute for challenges set but no challenge');
}








//End items in script

// General functions

//Function for getting elements by ID
function $() {
	return document.getElementById(arguments[0]);
}

//Insert element ahead of an existing element.
function insertAheadOf(newElement, anchorElement) {
	anchorElement.parentNode.insertBefore(newElement, anchorElement);
}

// Insert element after an existing element.
function insertAfter(newElement, anchorElement) {
	anchorElement.parentNode.insertBefore(newElement, anchorElement.nextSibling);
	}

//Show alert warning.
function fShowAlert(alertText) {
	var cdl = document.getElementsByClassName('CacheDetailsList')[0];
	var divNFP = $('divNFP');
	if (!divNFP) {
		divNFP = document.createElement('div');
		divNFP.id = 'divNFP';
		insertAheadOf(divNFP, cdl);
		divNFP.classList.add('nfw-warn');
		divNFP.classList.add('dropshadow_se');
	} else {
		divNFP.appendChild(document.createElement('br'));
	}
	var imgNFP = document.createElement('img');
	imgNFP.src = GeneralWarningImg;
	imgNFP.style.marginRight = '12px'
	divNFP.appendChild(imgNFP);
	divNFP.appendChild(document.createTextNode(alertText));
}

//Great Circle distance calculation. Distance returned in specified units.
function fDistCalc(lat1, lon1, lat2, lon2, units) {
		switch(units) {
			case 'mi':		erad = 3956.665;		break;
			case 'ft':		erad = 20891191.2;		break;
			case 'm':		erad = 6367635.08;		break;
			case 'km':		erad = 6367.63508;		break;
		}

		if (lat1-lat2==0 && lon1-lon2==0) {
			return 0;
		}

		//Convert to radians.
		lat1 = lat1 / 180 * Math.PI;
		lat2 = lat2 / 180 * Math.PI;
		var lonDelta = (lon2 - lon1) / 180 * Math.PI;
		//Base calculation.
		var gcWork = Math.acos(
			(Math.sin(lat1) * Math.sin(lat2)) +
			(Math.cos(lat1) * Math.cos(lat2) * Math.cos(lonDelta))
		)
		var dist = erad * gcWork;
		return dist;
}

//Open RAA in new window
function open_raa(){
	window.open('http://www.swedenreviewers.se/raa/konv1.asp?latlon=' + encodeURIComponent(txCoords) + '&Zoom=200&go=gorev&emergency=1');
}

//Create new element
function newElement() {
	if(arguments.length === 1) {
		return document.createTextNode(arguments[0]);
	} else {
		var newNode = document.createElement(arguments[0]),
			newProperties = arguments[1],
			prop, i;
		for (prop in newProperties) {
			if ((prop.indexOf('on') === 0) && (typeof newProperties[prop] === 'function')) {
				newNode.addEventListener(prop.substring(2), newProperties[prop], false);
			} else if (',innerHTML,outerHTML,textContent'.indexOf(','+prop) !== -1) {
				newNode[prop] = newProperties[prop];
			} else if ((',checked,disabled,selected'.indexOf(','+prop) !== -1) && !newProperties[prop]) {
				// value is false, which most browsers do not support, so don't set the property at all
			} else if (/\&/.test(newProperties[prop])) {
				newNode.setAttribute(prop, newProperties[prop].parseHTMLentities());
			} else {
				newNode.setAttribute(prop, newProperties[prop]);
			}
		}
		for (i=2; i<arguments.length; i++) {
			newNode.appendChild(arguments[i]);
		}
		return newNode;
	}
}

// End general functions





// Settings menu

// Show Edit Settings box.
function fEditSettings() {
	// If div already exists, reposition browser, and show alert.
	var divSet = document.getElementById("gm_divSet");
	if (divSet) {
		var YOffsetVal = parseInt(divSet.getAttribute('YOffsetVal', '0'));
		window.scrollTo(window.pageXOffset, YOffsetVal);
		alert('Edit Setting interface already on screen.');
		return;
	}

	// Create div.
	divSet = document.createElement('div');
	divSet.id = 'gm_divSet';
	divSet.setAttribute('style', 'position: absolute; ' +
		'padding: 5px; outline: 6px ridge blue; background: #FFFFCC;');
	var popwidth = parseInt(window.innerWidth * .5);
	divSet.style.width = popwidth + 'px';

	// Create heading.
	var ds_Heading = document.createElement('span');
	ds_Heading.style.fontSize = 'medium';
	ds_Heading.style.fontStyle = 'italic';
	ds_Heading.style.fontWeight = 'bold';
	ds_Heading.appendChild(document.createTextNode('"GCR SweReview v2" script settings'));
	divSet.appendChild(ds_Heading);

	// Show "Check additional waypoints"
	var pCaw = document.createElement('p');
	// pCaw.setAttribute('class', 'SettingElement');
	var cbCaw = document.createElement('input');
	cbCaw.id = 'cbCaw';
	cbCaw.type = 'checkbox';
	cbCaw.style.marginRight = '6px';
	var labelCaw = document.createElement('label');
	labelCaw.setAttribute('for', 'cbCaw');
	labelCaw.appendChild(document.createTextNode('Check additional waypoints.'));
	var Caw = GM_getValue('Caw' , false);
	cbCaw.checked = (Caw == true);
	pCaw.appendChild(cbCaw);
	pCaw.appendChild(labelCaw);
	divSet.appendChild(pCaw);

	// Show "Check cache against RAA"
	var pCcaR = document.createElement('p');
	// pCcaR.setAttribute('class', 'SettingElement');
	var cbCcaR = document.createElement('input');
	cbCcaR.id = 'cbCcaR';
	cbCcaR.type = 'checkbox';
	cbCcaR.style.marginRight = '6px';
	var labelCcaR = document.createElement('label');
	labelCcaR.setAttribute('for', 'cbCcaR');
	labelCcaR.appendChild(document.createTextNode('Check cache against RAA.'));
	var CcaR = GM_getValue('CcaR' , false);
	cbCcaR.checked = (CcaR == true);
	pCcaR.appendChild(cbCcaR);
	pCcaR.appendChild(labelCcaR);
	divSet.appendChild(pCcaR);

	// Show "If need archive"
	var pCina = document.createElement('p');
	// pCina.setAttribute('class', 'SettingElement');
	var cbCina = document.createElement('input');
	cbCina.id = 'cbCina';
	cbCina.type = 'checkbox';
	cbCina.style.marginRight = '6px';
	var labelCina = document.createElement('label');
	labelCina.setAttribute('for', 'cbCina');
	labelCina.appendChild(document.createTextNode('Check if cache needs archive.'));
	var Cina = GM_getValue('Cina' , false);
	cbCina.checked = (Cina == true);
	pCina.appendChild(cbCina);
	pCina.appendChild(labelCina);
	divSet.appendChild(pCina);

	// Show "Check time since last edit"
	var pCtsle = document.createElement('p');
	// pCtsle.setAttribute('class', 'SettingElement');
	var cbCtsle = document.createElement('input');
	cbCtsle.id = 'cbCtsle';
	cbCtsle.type = 'checkbox';
	cbCtsle.style.marginRight = '6px';
	var labelCtsle = document.createElement('label');
	labelCtsle.setAttribute('for', 'cbCtsle');
	labelCtsle.appendChild(document.createTextNode('Check time since last edit.'));
	var Ctsle = GM_getValue('Ctsle' , false);
	cbCtsle.checked = (Ctsle == true);
	pCtsle.appendChild(cbCtsle);
	pCtsle.appendChild(labelCtsle);
	divSet.appendChild(pCtsle);

	// Show "Check for first hide"
	var pCffh = document.createElement('p');
	// pCffh.setAttribute('class', 'SettingElement');
	var cbCffh = document.createElement('input');
	cbCffh.id = 'cbCffh';
	cbCffh.type = 'checkbox';
	cbCffh.style.marginRight = '6px';
	var labelCffh = document.createElement('label');
	labelCffh.setAttribute('for', 'cbCffh');
	labelCffh.appendChild(document.createTextNode('Check for first hide.'));
	var Cffh = GM_getValue('Cffh' , false);
	cbCffh.checked = (Cffh == true);
	pCffh.appendChild(cbCffh);
	pCffh.appendChild(labelCffh);
	divSet.appendChild(pCffh);

	// Create buttons.
	divSet.appendChild(document.createElement('p'));
	var ds_ButtonsP = document.createElement('p');
	ds_ButtonsP.style.textAlign = 'right';
	var ds_SaveButton = document.createElement('button');
	ds_SaveButton = document.createElement('button');
	ds_SaveButton.appendChild(document.createTextNode("Save"));
	ds_SaveButton.addEventListener("click", fSaveButtonClicked, true);
	var ds_CancelButton = document.createElement('button');
	ds_CancelButton.style.marginLeft = '6px';
	ds_CancelButton.addEventListener("click", fCancelButtonClicked, true);
	ds_CancelButton.appendChild(document.createTextNode("Cancel"));
	ds_ButtonsP.appendChild(ds_SaveButton);
	ds_ButtonsP.appendChild(ds_CancelButton);
	divSet.appendChild(ds_ButtonsP);

	// Add div to page.
	var toppos = parseInt(window.pageYOffset + 100);
	var leftpos = parseInt((window.innerWidth / 2) - (popwidth / 2));
	divSet.style.top = toppos + 'px';
	divSet.style.left = leftpos + 'px';
	divSet.setAttribute('YOffsetVal', window.pageYOffset);
	document.body.appendChild(divSet);

	//Save values.
	function fSaveButtonClicked() {

		//Save Check additional waypoints.
		var cbCaw = document.getElementById('cbCaw');
		GM_setValue('Caw' , (cbCaw.checked)?true:false);

		//Save Check cache against RAA.
		var cbCcaR = document.getElementById('cbCcaR');
		GM_setValue('CcaR' , (cbCcaR.checked)?true:false);

		// Save If need archive.
		var cbIfneedarchive = document.getElementById('cbCina');
		GM_setValue('Cina' , (cbCina.checked)?true:false);

		// Save Check time since last edit.
		var cbCtsle = document.getElementById('cbCtsle');
		GM_setValue('Ctsle' , (cbCtsle.checked)?true:false);

		// Save Check for first hide.
		var cbCffh = document.getElementById('cbCffh');
		GM_setValue('Cffh' , (cbCffh.checked)?true:false);

		alert('Settings have been saved.');
		fCloseSettingsDiv();
	}

	// Cancel request.
	function fCancelButtonClicked() {
		var resp = confirm('Any changes will be lost. Close Settings?');
		if (resp) {
			fCloseSettingsDiv();
		}
	}

	// Remove element and everything below it.
	function removeNode(element) {
		element.parentNode.removeChild(element);
	}

	// Close settings window
	function fCloseSettingsDiv() {
		var divBlackout = document.getElementById('divBlackout');
		var divSet = document.getElementById('gm_divSet');
		var winTopPos = divSet.getAttribute('winTopPos') - 0; // snapback code
		removeNode(divSet);
		removeNode(divBlackout);
		window.removeEventListener('resize', fSetLeftPos, true);
		document.documentElement.scrollTop = winTopPos; // snapback code
	}

	function fSetLeftPos() {
		var divSet = document.getElementById('gm_divSet');
			if (divSet) {
				var popwidth = parseInt(window.innerWidth * .5);
				divSet.style.width = popwidth + 'px';
				var leftpos = parseInt((window.innerWidth / 2) - (popwidth / 2));
				divSet.style.left = leftpos + 'px';
			}
		}
}

// End settings menu