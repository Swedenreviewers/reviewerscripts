/*
* v01.10 Add "Discover"-log remove

* v01.00 New Script. Remove Trackable-logs

// ==UserScript==
// @name        GCR Trackable Remover
// @namespace   dahlin.cx
// @description Removes all logs from trackables
// @include     http://*.geocaching.com/admin/review.aspx*
// @version     01.10
// @require     SweRevVerChk.js
// @icon        http://www.dahlin.cx/geocaching/swerev/greasemonkey/GCR_SweRev/swefrog.png
// ==/UserScript==
*/


// ------------------------Version Checking------------------------ //
fCheckScriptVersion('c05b328e-d976-47d5-8eda-04ff55d7d979', '01.10');
// ------------------------Version Checking------------------------ //

var ary = ["13.png", "14.png", "75.png", "48.png"];
var trHtml="";
var item;
var o=document.getElementsByTagName("tr");

for (var i = o.length - 1; i >= 0; i--){
	item=o.item(i);

	trHtml=item.innerHTML;

	for (var z = ary.length - 1; z >= 0; z--){
		if(trHtml.search(ary[z])!=-1){
			item.style.display="none";
			a=i+1;
			item=o.item(a);
			item.style.display="none";
            }		
	};
};
