/*
Version history
* v02.20				Change from online date to last active date
* v02.11				Corrected error in last edit
* v02.10				Added function for use of native email client
* v02.00				New version for new profile page
* v01.00				New Version of Enhancement to the Queue page for Sweden. Version 2

// ==UserScript==
// @name				GCR SweReview v2 Time since last visit
// @description			Enhancements to the profile page
// @namespace			https://admin.geocaching.com
// @author				Sweden Reviewers Toa Norik
// @version				02.20
// @icon				https://i.imgur.com/zA6Y7hV.png
// @updateURL			https://gitlab.com/Swedenreviewers/reviewerscripts/raw/master/GCR_SweReview_v2_Time_since_last_visit.user.js
// @downloadURL			https://gitlab.com/Swedenreviewers/reviewerscripts/raw/master/GCR_SweReview_v2_Time_since_last_visit.user.js
// @include				http*://www.geocaching.com/profile/*
// @include				http*://www.geocaching.com/p*
// @grant				GM_getValue
// @grant				GM_setValue
// ==/UserScript==
*/

// Activate settings menu


//******************* DOM elements and variables ************************//
	// Pre-acquire needed DOM elements.
	//var e_lastvisit					= document.getElementById('ctl00_ContentBody_ProfilePanel1_lblLastVisitDate');

    var e_lastvisit					= document.getElementById('lblLastActivityDate');


// Get information

	var acttime						= new Date();
	var COLastOnline				= e_lastvisit.innerHTML;
	var res							= COLastOnline.split(" ");
		COLastOnline				= (res[2]);
		COLastOnline				= COLastOnline.substring(0,19);
		res							= COLastOnline.split("-");
	var year						= res[0];
	var month						= res[1];
	var day							= res[2];


	//This date should be set to "history point".
	//var refdate						= GM_getValue("refdate");
	var refdate						= new Date("January 11, 2024 00:00:00");				//Since last sweep
	//var inactiveuserdate			= acttime - 31536000000;							//1 year ago
	//var inactiveuserdate			= acttime - 15768000000;							//6 month ago
	var inactiveuserdate			= acttime - 23652000000;							//9 month ago
	var daysincerfdt				= Math.floor((acttime-refdate)/86400000);			//Days since lastonline
	var daysinceiudt				= Math.floor((acttime-inactiveuserdate)/86400000);	//Days to call inactive user
	var str							= window.location.href;
		res							= str.match(/close=y/g);

	switch (month) {
		case "01":
			month = 0
			break;
		case "02":
			month = 1
			break;
		case "03":
			month = 2
			break;
		case "04":
			month = 3
			break;
		case "05":
			month = 4
			break;
		case "06":
			month = 5
			break;
		case "07":
			month = 6
			break;
		case "08":
			month = 7
			break;
		case "09":
			month = 8
			break;
		case "10":
			month = 9
			break;
		case "11":
			month = 10
			break;
		case "12":
			month = 11
			break;
	}

	var WpCOLastOnlineTime			= new Date(year,month,day);
	var his							= (Math.floor((acttime - WpCOLastOnlineTime)/86400000));


// End getting information

// Items in script

	//Check if "close" is chosen ; then close window if criteria is met.
	if (res) {
		if (his > daysincerfdt && his < daysinceiudt){			//If user has NOT been online since reference date or if user HAS been online since reference date.
			window.close();
		}
	}
	//

	if (his > 365) {
		var ar = Math.floor(his/365);
		//e_lastvisit.style.backgroundColor = 'yellow';
		var emDateDif = document.createElement('em');
		emDateDif.style.fontWeight = 'bold';
		emDateDif.style.color = 'red';
		insertAfter(emDateDif, e_lastvisit);
		emDateDif.appendChild(document.createTextNode(' \u2190' +
					'User was active over ' + ar + ' year(s) ago. (' + his + ' days)'));
	} else if (his == 0) {
		//e_lastvisit.style.backgroundColor = 'yellow';
		emDateDif = document.createElement('em');
		emDateDif.style.fontWeight = 'bold';
		emDateDif.style.color = 'red';
		insertAfter(emDateDif, e_lastvisit);
		emDateDif.appendChild(document.createTextNode(' \u2190' +
					'User was active today.'));
	} else if (his > 30) {
		var manader = Math.floor(his/30);
		//e_lastvisit.style.backgroundColor = 'yellow';
		emDateDif = document.createElement('em');
		emDateDif.style.fontWeight = 'bold';
		emDateDif.style.color = 'red';
		insertAfter(emDateDif, e_lastvisit);
		emDateDif.appendChild(document.createTextNode(' \u2190' +
					'User was active over ' + manader + ' month(s) ago. (' + his + ' days)'));
	} else if (his > 21) {
		//e_lastvisit.style.backgroundColor = 'yellow';
		emDateDif = document.createElement('em');
		emDateDif.style.fontWeight = 'bold';
		emDateDif.style.color = 'red';
		insertAfter(emDateDif, e_lastvisit);
		emDateDif.appendChild(document.createTextNode(' \u2190' +
					'User was active over 3 weeks ago. (' + his + ' days)'));
	} else if (his > 14) {
		//e_lastvisit.style.backgroundColor = 'yellow';
		emDateDif = document.createElement('em');
		emDateDif.style.fontWeight = 'bold';
		emDateDif.style.color = 'red';
		insertAfter(emDateDif, e_lastvisit);
		emDateDif.appendChild(document.createTextNode(' \u2190' +
					'User was active over 2 weeks ago. (' + his + ' days)'));
	} else if (his > 7) {
		//e_lastvisit.style.backgroundColor = 'yellow';
		emDateDif = document.createElement('em');
		emDateDif.style.fontWeight = 'bold';
		emDateDif.style.color = 'red';
		insertAfter(emDateDif, e_lastvisit);
		emDateDif.appendChild(document.createTextNode(' \u2190' +
					'User was active over 1 week ago. (' + his + ' days)'));
	} else {
		//e_lastvisit.style.backgroundColor = 'yellow';
		emDateDif = document.createElement('em');
		emDateDif.style.fontWeight = 'bold';
		emDateDif.style.color = 'red';
		insertAfter(emDateDif, e_lastvisit);
		emDateDif.appendChild(document.createTextNode(' \u2190' +
					'User was active ' + his + ' day(s) ago.'));
			}

//Switch to nativ email-page
	// Declare global variables.
	var UserGuid;
	var UserName;
	var email;
	// Get User GUID and Name.
	var e_HomeLink = document.getElementById("ctl00_ProfileHead_ProfileHeader_lblMemberName");
	UserName = e_HomeLink.firstChild.data;
	UserName = UserName.replace(/<\&amp;>/g, '&');
	var messageLink = document.getElementById("ctl00_ProfileHead_ProfileHeader_lnkSendMessage");
	if (messageLink) {
		UserGuid = UrlParm('guid', true, e_HomeLink.href);
	}
	email="https://www.geocaching.com/email/?guid=" + UserGuid;
	document.getElementById("ctl00_ProfileHead_ProfileHeader_lnkSendEmailToEmail").href = email;


//End items in script

// General functions

//	Insert element after an existing element.
	function insertAfter(newElement, anchorElement) {
		anchorElement.parentNode.insertBefore(newElement, anchorElement.nextSibling);
	}

	// Returns a URL parameter.
	//  ParmName - Parameter name to look for.
	//  IgnoreCase - (optional) *false, true. Ignore parmeter name case.
	//  UrlString - (optional) String to search. If omitted, document URL is used.
	function UrlParm(ParmName, IgnoreCase, UrlString) {
        var RegRslt, sc = '', RtnVal = '';
		if (IgnoreCase) {sc = 'i'}
		if(UrlString) {
			var PageUrl = UrlString;
		} else {
			PageUrl = document.location + '';
		}

		var ParmString = PageUrl.substring(PageUrl.indexOf('?') + 1);
		var RegEx1 = new RegExp('(^|&)' + ParmName + '=(.*?)(&|#|$)', sc);
		RegRslt = RegEx1.exec(ParmString);
		if (RegRslt) {RtnVal = RegRslt[2]}

		return RtnVal;
	}


// End general functions

// Settings menu
// End settings menu
