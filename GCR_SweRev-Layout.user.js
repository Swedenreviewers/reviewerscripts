// ==UserScript==
// @name			GCR_SweRev:Layout
// @description		Enhancements to the GC Reviewer page
// @namespace		jonas
// @version			02.00
// @icon			http://i.imgur.com/a94PYHJ.png
// @updateURL		https://gitlab.com/Swedenreviewers/reviewerscripts/raw/master/GCR_SweRev-Layout.user.js
// @downloadURL		https://gitlab.com/Swedenreviewers/reviewerscripts/raw/master/GCR_SweRev-Layout.user.js
// @resource		litmusCSS1 https://admin.geocaching.com/Content/geoadmin?v=xkfBdcXBektkFAxt0aOF42Vbu0JEF-vKdobvSrvKQaI1
// @resource		litmusCSS2 https://gitlab.com/geocachinghq/reviewer_scripts/raw/master/CSS/gcr_Review_Page.txt
// @include			http*://*.geocaching.com/admin/review.aspx*
// @grant			GM_addStyle
// ==/UserScript==



// Enhancement switches.
// Set value to 0 (zero) to turn off, or 1 (one) to turn on.
var Switch_RevArcLink = 1;		// Show Review Note and Archive/Unarchive links

// Get current domain;
var domain = location.protocol + '//' + location.hostname;

// Pre-acquire needed DOM elements.
var e_CacheDetails_Container = document.getElementById('ctl00_ContentBody_CacheDetails_Container');
var e_CacheDetails_lbHolder = document.getElementById('ctl00_ContentBody_CacheDetails_lbHolder');
var e_CacheDetails_Location = document.getElementById('ctl00_ContentBody_CacheDetails_Location');
var e_CacheDetails_Name = document.getElementById('ctl00_ContentBody_CacheDetails_Name');
var e_CacheDetails_Status = document.getElementById('ctl00_ContentBody_CacheDetails_Status');
var e_lnkEdit = document.getElementById('ctl00_ContentBody_lnkEdit');
var e_lnkLog = document.getElementById('ctl00_ContentBody_lnkLog');


var GpxImg =
 "data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAB0AAAAMCAYAAACeGbYxAAAA" +
 "AXNSR0IArs4c6QAAAARnQU1BAACxjwv8YQUAAAAgY0hSTQAAeiYAAICEAAD6AAAAgOgAAH" +
 "UwAADqYAAAOpgAABdwnLpRPAAAABh0RVh0U29mdHdhcmUAUGFpbnQuTkVUIHYzLjEwcrIl" +
 "kgAAAc5JREFUOE%2BtlN3KgUEQx9epUk5cl2twCa7ADbgB5y7AsVNJIp%2FlMyQhhchXku" +
 "Z9fqvZtofeg%2Fel9tmxzcz%2FP%2F%2BZ3YiIGP0dj0fZ7XbmcrmYwHbnfzVisZiJRqMm" +
 "Ho%2BbRCIRcXkAZc1mM6lUKsK%2B2Wzker3%2Be5FnPp9LrVaTXq8nj8cjgAqK5DOZTKTb" +
 "7crpdJJisSipVEqSyaTdc7mcA8%2Fn8%2FYsnU67hT8E8ctms843UMz6LBYLOZ%2FP0u%2" +
 "F3pdPpvEAPh4NUq1UHCNhwOHTBmUzGJSOxT6JcLlty6g8hziBBnO%2FLWaPRsCTMaDSyBo" +
 "cEKXOVF8YaHAbVmGazaePZVR2qDLcIuQE2fLbbrXWA9W%2B9VAlJzoIgMUpaZQ6faU7ahx" +
 "KGJiPxJ1DtnZIBVHtNRUjot0Irx6dQKLwVQG9LpZIYpmq1WlkHQMLy%2BmQ%2ByesrwyBB" +
 "hMrDs4EfilKkWS6XVioOdTB0GHy51A4Ph4ISQ%2FX6X2%2BBT4oJZobslWm1WjKdTh0wFc" +
 "OUnWsCe4KxWZ%2F6jq9PFh8qV%2F%2F1em1vyf1%2Bf4Hebjep1%2BvSbrfdUH3jcSDHfr" +
 "%2BX8XhsHx5s9zhgPJ9P%2BxqhOc3%2B1qK6wWBgK9TX7weio0kZaGyb9gAAAABJRU5Erk" +
 "Jggg%3D%3D";



// Text checking function.
String.prototype.startsWith = function(str) {
 return (this.indexOf(str) == 0);
}

String.prototype.contains = function(str) {
 return (this.indexOf(str) >= 0);
}


//String.prototype.endsWith = function(str) {
// return (this.lastIndexOf(str) == this.length() - str.length());
//}

// Get currently signed-on geocaching.com profile.
var SignedInAs = document.getElementById("ctl00_LoginUrl");
if (!SignedInAs) { SignedInAs = document.getElementById('ctl00_LogoutUrl'); }
SignedInAs = SignedInAs.parentNode.childNodes[1].firstChild.data.trim();
SignedInAs = SignedInAs.replace(/<\&amp;>/g, '&');


// Resize windows > 600px on load.
 GM_addStyle(".gm-img-resizer { width: 600px !important; height: auto !important; } ");
 window.addEventListener("load", fResizeImages, false);


// Remove unnecessary white space.
var PathSearch = "//a[contains(@href, 'queue.aspx')]";
var wsLinks = document.evaluate(PathSearch, document, null,
 XPathResult.ORDERED_NODE_SNAPSHOT_TYPE,	null);
if(wsLinks.snapshotLength > 0) {
 var wsLink = wsLinks.snapshotItem(0);
 wsLink.parentNode.height = 28;
}

// Get cache data element.
var CacheData = document.getElementById('ctl00_ContentBody_CacheDataControl1_CacheData');
if (!CacheData) {
 CacheData = document.getElementsByClassName('CacheData')[0];
}



// Get the GUID for the cache, if needed.
var CacheGuid = CacheData.getAttribute('data-cacheguid');

// Get cache waypoint id.
var WptID = CacheData.getAttribute('data-gccode');

// Get decimal-degree coordinate values.
var CoordDdLat = CacheData.getAttribute('data-latitude') - 0;
var CoordDdLon = CacheData.getAttribute('data-longitude') - 0;


// Get Cache Title.
var CacheName = e_CacheDetails_Name.firstChild;
while (CacheName.nodeName != '#text') {
 CacheName = CacheName.firstChild;
}
CacheName = CacheName.data;
CacheName = CacheData.getAttribute('data-cachename');

// Get owner name & guid.
var OwnerName = CacheData.getAttribute('data-ownerusername');
var OwnerGuid = CacheData.getAttribute('data-ownerguid');

// Get coordinates for this cache.
var e_CacheDetails_Coords = document.getElementById("ctl00_ContentBody_CacheDetails_Coords");
var txCoordsNode = e_CacheDetails_Coords.firstChild;
while (txCoordsNode.nodeName != '#text') {
 txCoordsNode = txCoordsNode.firstChild;
}
var txCoords = txCoordsNode.data;



// Change cache type cell to fixed size.
document.getElementById('cTypeImg').style.width = '60px';

// Remove extra line above cache title.
e_CacheDetails_Name.parentNode.style.marginTop = '0px';

// Remove extra line above profile name.
var e_lbUsername = document.getElementById("ctl00_ContentBody_UserInfo_lbUsername");
e_lbUsername.parentNode.parentNode.style.marginTop = '0px';

// Reduce size of type/size cell.
var e_CacheDetails_WptType = document.getElementById("ctl00_ContentBody_CacheDetails_WptType");
e_CacheDetails_WptType.parentNode.parentNode.style.width = '100px';


// Code for cache size.
var CacheSize1 = e_CacheDetails_Container.innerHTML;
CacheSize1 = CacheSize1.substr(CacheSize1.indexOf('(') + 1, 1);





// Get cache status.
var bPublished = false;
var bDisabled = false;
var bArchived = false;
var bHeld = false;
var bWatched = false;

if (e_CacheDetails_Status) {
 var sCacheStatus = e_CacheDetails_Status.firstChild.data;
  bArchived = (sCacheStatus.contains('Archived'));
  bDisabled = (sCacheStatus.contains('Inactive'));
  bPublished = (!(sCacheStatus.contains('Not Published')));
} else {
 bPublished = true;
}
if (e_CacheDetails_lbHolder) { bHeld = true; }




/*
// If published, add Update Coordinates link next to Edit Listing.
var lnkEditDD = e_lnkEdit.parentNode
if (bPublished) {
 lnkEditDD.appendChild(document.createTextNode(' | '));
 var lnkUpdateCoords = document.createElement("A");
 lnkUpdateCoords.href = '../seek/log.aspx?' +
 'LogType=47' +
 '&WP=' + WptID +
 '&title=' + encodeURIComponent(CacheName.trim()) +
 '&owner=' + encodeURIComponent(OwnerName) +
 '&coords=' + encodeURIComponent(txCoords) +
 '#ctl00_ContentBody_LogBookPanel1_tbLogInfo';
 lnkUpdateCoords.target = "_blank";
 lnkUpdateCoords.appendChild(document.createTextNode('Update Coordinates'));
 lnkEditDD.appendChild(lnkUpdateCoords);
}
*/

// Remove class from cache page link, to remove warning color.
document.getElementById("ctl00_ContentBody_lnkListing").setAttribute('class', '');

// Locate hold
var e_hold = document.getElementById('ctl00_ContentBody_CacheDetails_holdStatus');
var e_watch = document.getElementById('ctl00_ContentBody_CacheDetails_watchStatus');
//e_hold.innerHTML = "On Hold";
//e_watch.innerHTML = "You are watching the cache";
//document.getElementById("ctl00_ContentBody_CacheDetails_watchStatus").style.display = "none";
//document.getElementById("ctl00_ContentBody_CacheDetails_holdStatus").style.display = "none";

var nplace = document.getElementById('options');
//if (e_watch) {
//e_watch.style.display = "none"; }
//if (e_hold) {
//e_hold.style.display = "none"; }

// Set Review Log type code.
var ReviewLogType = '18';
if (bPublished) { ReviewLogType = '68'; }



// Add enhancements to Navagation box.
// Locate navagation box table.
 PathSearch = "//img[contains(@src, 'view_cache_listing.png')]";
var gifBhandList = document.evaluate(PathSearch, document, null,
 XPathResult.ORDERED_NODE_SNAPSHOT_TYPE,	null);
if (gifBhandList.snapshotLength == 1) {
 var gifBhand = gifBhandList.snapshotItem(0);

	 var nvxDiv = fUpGen(gifBhand, 3);
		nvxDiv.style.backgroundColor	= 'rgb(230, 240, 140)';

	var e_watchbutton	= document.getElementById('ctl00_ContentBody_watchButton');
	var e_holdButton	= document.getElementById('ctl00_ContentBody_holdButton');
	var e_lnkRNote		= document.getElementById('ctl00_ContentBody_lnkRNote');

		e_lnkRNote.style.fontWeight="bold";
		e_lnkRNote.style.color = 'blue';

		if (e_watchbutton.innerHTML == 'Stop watching') {
			e_watchbutton.style.fontWeight="bold";
			e_watchbutton.style.color = 'red';	}
		if(e_holdButton) {
			if (e_holdButton.value == 'Release hold') {
			e_holdButton.style.fontWeight="bold";
			e_holdButton.style.color = 'red';	}
		}

	var e_bottomBtnGroup	= document.getElementById('ctl00_ContentBody_bottomBtnGroup');
	if (e_bottomBtnGroup) {
		insertAfter(e_bottomBtnGroup, nplace);	}

	var e_publishButton		= document.getElementById('ctl00_ContentBody_publishButton');
	if (e_publishButton) {
	e_publishButton.style.width = '80px';}

  nvxDiv = fUpGen(gifBhand, 4);
 var h3RevOpt = nvxDiv.childNodes[1].childNodes[1];

 // Change width of navigation div.
 var leftDiv = nvxDiv.parentNode.childNodes[1];
 fResizeLeftDiv();
 nvxDiv.style.width = '290px';
 nvxDiv.style.lineHeight = '1.7em';


 window.addEventListener('resize', fResizeLeftDiv, false);


}

// Add links to post Review Note and Archive/Unarchive cache.
if (Switch_RevArcLink) {
 // Check current cache status.
 if (bArchived) {
 // Change title-box background to light red if archived.
 var td_e_CacheDetails_Status = fUpGenToType(e_CacheDetails_Status, 'TD');
 td_e_CacheDetails_Status.style.backgroundColor = 'rgb(227, 192, 192)';
 }
 if (bDisabled) {
 // Change title-box background to grey if inactive.
 if (!bArchived) {
 td_e_CacheDetails_Status = fUpGenToType(e_CacheDetails_Status, 'TD');
 td_e_CacheDetails_Status.style.backgroundColor = 'rgb(200, 200, 200)';
 }
 }


}


// Create GPX icon button.
var lnkGpx = document.createElement('a');
lnkGpx.id = 'lnkGpx'
lnkGpx.href = domain + '/admin/gengpx.aspx?guid=' + CacheGuid;
lnkGpx.title = 'Download GPX file for ' + WptID;
var imgGpx = document.createElement('img');
imgGpx.src = GpxImg;
imgGpx.classList.add('gm-icon-buttons');
lnkGpx.appendChild(imgGpx);
document.getElementById("ctl00_ContentBody_CacheDetails_downloadGpxBtn").style.display = "none";



// Put all links and text into a Span element.
var PageLinkSpan = document.getElementById("PageLinkSpan");
if (!PageLinkSpan) {
 PageLinkSpan = document.createElement("span");
 PageLinkSpan.id = 'PageLinkSpan';
 PageLinkSpan.appendChild(document.createElement('br'));
 insertAfter(PageLinkSpan, e_CacheDetails_Location);
}

// Add elements to span.
PageLinkSpan.appendChild(lnkGpx);



//****************************************************************************************//
// //
// Functions //
// //
//****************************************************************************************//



// Resize images > 600px.
function fResizeImages() {
 fResizeIn('ctl00_ContentBody_CacheDetails_ShortDesc');
 fResizeIn('ctl00_ContentBody_CacheDetails_LongDesc');
 function fResizeIn(domId) {
 var domElem = document.getElementById(domId);
 if (domElem) {
 // Get list of images.
 var imgs = document.evaluate(".//img", domElem, null,
 XPathResult.UNORDERED_NODE_SNAPSHOT_TYPE, null);
 // Loop through list.
 var ic = imgs.snapshotLength;
 for (var i = 0; i < ic; i++) {
 var pix = imgs.snapshotItem(i);
 var pixWid = pix.width - 0 ;
 if (pixWid > 600) {
 pix.removeAttribute('height');
 pix.removeAttribute('width');
 pix.classList.add("gm-img-resizer");
 }
 }
 }
 }
}



// Returns a URL parameter.
// ParmName - Parameter name to look for.
// IgnoreCase - (optional) *false, true. Ignore parmeter name case.
// UrlString - (optional) String to search. If omitted, document URL is used.
function UrlParm(ParmName, IgnoreCase, UrlString) {
 var RegRslt, sc = '', RtnVal = '';
 if (IgnoreCase) {sc = 'i'}
 if(UrlString) {
 var PageUrl = UrlString;
 } else {
 PageUrl = document.location + '';
 }
 var ParmString = PageUrl.substring(PageUrl.indexOf('?') + 1);
 var RegEx1 = new RegExp('(^|&)' + ParmName + '=(.*?)(&|#|$)', sc);
 RegRslt = RegEx1.exec(ParmString);
 if (RegRslt) {RtnVal = RegRslt[2]}
 return RtnVal;
}


// Resize left div element according to window size.
function fResizeLeftDiv() {
 var win = document.defaultView;
 if (win) {
 var zbody = document.getElementsByTagName('body')[0];
 var bodyelem = window.getComputedStyle(zbody, null);
 var docWidth = bodyelem.width.replace(/px/i,'') -0;
 var newWidth = docWidth - 356;
 if (newWidth < 604) { newWidth = 604; }
 newWidth = newWidth.toString() + 'px';
 leftDiv.style.width = newWidth;
 }
}

// Remove element and everything below it.
function removeNode(element) {
 element.parentNode.removeChild(element);
}

// Insert element after an existing element.
function insertAfter(newElement, anchorElement) {
 anchorElement.parentNode.insertBefore(newElement, anchorElement.nextSibling);
}


// Move up the DOM tree a specific number of generations.
function fUpGen(gNode, g) {
 for (var i = 0; i < g; i++) {
 gNode = gNode.parentNode;
 if (gNode.nodeName == 'undefined') {
 gNode = null;
 break;
 }
 }
 return gNode;
}

// Move up the DOM tree until a specific DOM type is reached.
function fUpGenToType(gNode, gType) {

 gType = gType.toUpperCase();
 while (gNode.nodeName.toUpperCase() != gType) {
 gNode = gNode.parentNode;
 if (!gNode) {
 throw false;
 break;
 }
 }
 return gNode;
}

