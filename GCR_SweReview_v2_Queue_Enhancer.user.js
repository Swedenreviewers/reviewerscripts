/*
Version history
* v01.00				New Version of Enhancement to the Queue page for Sweden. Version 2

// ==UserScript==
// @name				GCR SweReview v2 Queue Enhancer
// @description			Enhancements to the GC Queue page for Swedish Reviewers
// @include				http*://www.geocaching.com/admin/queue.aspx*
// @include				http*://staging.geocaching.com/admin/queue.aspx*
// @include				http*://www.geocaching.com/admin/review.aspx*
// @namespace			https://admin.geocaching.com
// @author				Sweden Reviewers Toa Norik
// @version				01.00
// @icon				https://i.imgur.com/zA6Y7hV.png
// @updateURL			https://gitlab.com/Swedenreviewers/reviewerscripts/raw/master/GCR_SweReview_v2_Queue_Enhancer
// @downloadURL			https://gitlab.com/Swedenreviewers/reviewerscripts/raw/master/GCR_SweReview_v2_Queue_Enhancer
// @grant				GM_getValue
// @grant				GM_setValue
// @grant				GM_addStyle
// @grant				GM_registerMenuCommand
// ==/UserScript==
*/

// Activate settings menu

	GM_registerMenuCommand('Edit "SWERev Queue Enhancer" Script Settings', fEditSettings);

//******************* DOM elements and variables ************************//
	// Pre-acquire needed DOM elements.

	// Set variables

	var SWEREW_QE_TimeDiff		= parseInt(GM_getValue("SWEREW_QE_TimeDiff", "9"));
	var Sws						= GM_getValue("Sws", false);
	var Ss						= GM_getValue("Ss", false);
	var ds_ColorOpt				="";
	var ColorSel_Sws			= GM_getValue("ColorSel_Sws", "Clu_classG");
	var ColorSel_N				= GM_getValue("ColorSel_N", "Clu_classG");
	var ColorSel_U				= GM_getValue("ColorSel_U", "Clu_classR");
	var ColorSel_OtXd			= GM_getValue("ColorSel_OtXd", "Clu_classR");
	var ColorSel_OtYd			= GM_getValue("ColorSel_OtYd", "Clu_classR");
	var color					= [];
		color[0]				="red";
		color[1]				="green";
		color[2]				="blue";
	var colorvalue				= [];
		colorvalue[0]			="Clu_classR";
		colorvalue[1]			="Clu_classG";
		colorvalue[2]			="Clu_classB";
	var PageUrl					= document.location.href;
	var page					= "";

	// ---------------------------------------------------------------- //
	// --------------------------- SETTINGS --------------------------- //
	// ---------------------------------------------------------------- //

	// Json keys:
	// Local Storage key for JSON tracking data.

	const JSON_LAST_UPDATE_DATE = '3fe9d532-72c0-48ba-9438-4dede6692438';

	// *****************************************************************
	// Initialize JSON object if missing (first time running)
	// *****************************************************************
	var jsonCluString = window.localStorage.getItem(JSON_LAST_UPDATE_DATE);
	//alert ('Init' + jsonCluString);
	if (jsonCluString == null) {
		//alert ('Init');
		var jsObj = new Object;
		jsonCluString = JSON.stringify(jsObj);
		window.localStorage.setItem(JSON_LAST_UPDATE_DATE, jsonCluString);
		delete jsObj;
	}

	// Number of hours between DB Cleanup (removal of old cache posts containing locked and last_looked_at data)
	const CLEANUP_DB_PERIOD = 72

	// Number of days to keep cache data after they was last looked at
	const KEEP_DAYS = 45

	// Styles

	// Create class for Update info.
	// Dodger Blue
	GM_addStyle(".Clu_classB { margin-left: 8px; background-color:#1e90ff; font-weight:bold; color: #FFFFFF }");
	// Forest Green
	GM_addStyle(".Clu_classG { margin-left: 8px; background-color:#6ea131; font-weight:bold; color: #FFFFFF }");
	// Red
	GM_addStyle(".Clu_classR { margin-left: 8px; background-color:#FF3333; font-weight:bold; color: #FFFFFF }");
	// Create class for 50% opaque.
	GM_addStyle(".gms_faded { opacity:0.35; }");

	// Images

// Get information

	// Determine if running review queue, Log post or review page.
	if (PageUrl.indexOf('review') >= 0) {
		page = 'R';
	} else if (PageUrl.indexOf('queue') >= 0) {
		page = 'Q';
	} else if (PageUrl.indexOf('bookmark') >= 0) {
		page = 'B';
	} else if (PageUrl.indexOf('CachePublish') >= 0) {
		page = 'B';
	} else if (PageUrl.indexOf('log') >= 0) {
		page = 'L';
	} else {
		return;
	}

// End getting information

// Items in script

	// Do DB cleanup every configured hour
	// Get the last time cleanup Was done.
	var LastDBCheck = GM_getValue('LastSweRevDBCheck', 0) - 0;
	var d = new Date();
	var MinutesNow = Math.floor(d.getTime() / 60000)
	// If at least selected hours since last check, do DB cleanup.
	var HoursSince = Math.floor((MinutesNow - LastDBCheck) / 60);
	if (HoursSince >= CLEANUP_DB_PERIOD) {
		// Remove posts older than selected days
		CleanOldDBPosts(KEEP_DAYS);
		// Update cleanup time;
		GM_setValue('LastSweRevDBCheck', MinutesNow);
	}

	if (page == 'Q') {
		// Updates for the Queue page

		// Get number of caches in filter
		var e_NumberOfCachesNode = document.evaluate("/html/body/form/div[3]/div[2]/div/div[2]/table[1]/tbody/tr/td[1]/span/b[1]",
		document, null, XPathResult.FIRST_ORDERED_NODE_TYPE, null);

		// If message about site maint, then node will be null
		try {
			var NumberOfCaches = e_NumberOfCachesNode.singleNodeValue.innerHTML;
		} catch (err) {
			e_NumberOfCachesNode = document.evaluate("/html/body/form/div[2]/div[3]/div/div[2]/table/tbody/tr/td/span/b",
			document, null, XPathResult.FIRST_ORDERED_NODE_TYPE, null);
			try {
				NumberOfCaches = e_NumberOfCachesNode.singleNodeValue.innerHTML;
			} catch (err) {
				// probably on staging
				e_NumberOfCachesNode = document.evaluate("/html/body/form/div[3]/div[3]/div/div[2]/table[1]/tbody/tr/td[1]/span/b[1]",
				document, null, XPathResult.FIRST_ORDERED_NODE_TYPE, null);
				NumberOfCaches = e_NumberOfCachesNode.singleNodeValue.innerHTML;
			}
		}

		// Update cachestatus
		fLoadAllCacheDates();

	}

	if (page == 'R') {
	// If page is a review page; update last read date
	// Update when user looked at cache.
		var CacheData = document.getElementById('ctl00_ContentBody_CacheDataControl1_CacheData');
		var wptid = CacheData.getAttribute('data-cacheguid');
		var jsCluObj = new Object;
		jsonCluString = window.localStorage.getItem(JSON_LAST_UPDATE_DATE);
		jsCluObj = JSON.parse(jsonCluString);
		// Update timenote.
		var LastLooked = new Date();
		// add an extra 3 minutes to compensate for time differences
		LastLooked.setTime(LastLooked.getTime() + 180000);
		jsCluObj[wptid] = {
			'date': LastLooked,
			'locked': CacheData.getAttribute('data-islocked')
		};
		// Save to local storage for access by other scripts.
		jsonCluString = JSON.stringify(jsCluObj);
		window.localStorage.setItem(JSON_LAST_UPDATE_DATE, jsonCluString);
		delete jsCluObj;
	}

// End items in script

// General functions

	function removeNode(element) {
		// Remove element and everything below it.
		element.parentNode.removeChild(element);
	}

	function CleanOldDBPosts(LastLookedAge) {
		// Funtion used to remove old post in the CacheDB.
		// Will Itterate through JSON and remove posts older than LastLookedAge in days
		// console.log ('Starting DbCleanup...\n');
		// Open database

		var jsonCluString1 = window.localStorage.getItem(JSON_LAST_UPDATE_DATE);
		var jsCluObj1 = new Object;
		jsCluObj1 = JSON.parse(jsonCluString1);

		// Store time to check against
		var AgeLimit = new Date() - LastLookedAge*86400000;
		var NumberOfPosts = 0;
		var DeletedPosts = 0;
		for (var key in jsCluObj1) {
		if (!jsCluObj1.hasOwnProperty(key)) continue;
			NumberOfPosts = NumberOfPosts + 1;
			//console.log(NumberOfPosts);
			var obj = jsCluObj1[key];

			//console.log('Date = ' + obj['date']);

			if (new Date(obj.date) < AgeLimit) {
				// Is longer than set time since being viewed. Delete it
				//console.log ('Wp is being deleted. Date is ' + obj['date'] + ' Age set is ' + LastLookedAge + ' days.\n' );
				DeletedPosts = DeletedPosts + 1;
				delete jsCluObj1[key];
			}
		}

		// Save database
		jsonCluString1 = JSON.stringify(jsCluObj1);
		// console.log('Resulting string= ' + jsonCluString1);
		localStorage.removeItem(JSON_LAST_UPDATE_DATE);
		window.localStorage.setItem(JSON_LAST_UPDATE_DATE, jsonCluString1);
		delete jsCluObj1;

		var jsonCluString2 = window.localStorage.getItem(JSON_LAST_UPDATE_DATE);
		//console.log ('Stored string after cleanup: ' + jsonCluString2);
		//console.log ('Number of posts before cleanup: ' + NumberOfPosts + ', Deleted posts: ' + DeletedPosts + ', Remaining posts: ' + (NumberOfPosts - DeletedPosts));
		alert ('DB cleanup completed. Number of posts before cleanup: ' + NumberOfPosts + ', Deleted posts: ' + DeletedPosts + ', Remaining posts: ' + (NumberOfPosts - DeletedPosts));
	}

	function fLoadAllCacheDates() {
		// Load/refresh all changetimes. Called on page load of queue page, and when
		// the JSON data has been changed by another process.
		// Retrieve JSON database.
		var jsCluObj = new Object;
		var jsonCluString = window.localStorage.getItem(JSON_LAST_UPDATE_DATE);
		if (jsonCluString) {
			jsCluObj = JSON.parse(jsonCluString);
		}

		// Loop through list of caches.
		var elems = document.getElementsByClassName('CacheData');
		var ic = elems.length;
		for (var e = 0; e < ic; e++) {
			var wptid = elems[e].getAttribute('data-gccode');
			var wpwatch = elems[e].getAttribute('data-iamwatching');
			var wpguid = elems[e].getAttribute('data-cacheguid');
			var WpLastUpdated = elems[e].getAttribute('data-datelastupdated');

			// Check if cache is held
			var CacheHolder=elems[e].getAttribute('data-holderguid');
			if (CacheHolder) {
				var CacheIsHeld = true;
			} else {
				CacheIsHeld = false;
			};

			var year = WpLastUpdated.substring(0, 4);
			var month = WpLastUpdated.substring(4, 6) - 1;
			var day = WpLastUpdated.substring(6, 8);
			var hour = WpLastUpdated.substring(8, 10);
			var minute = WpLastUpdated.substring(10, 12);
			var sec = WpLastUpdated.substring(12, 14);
			var WpLastUpdatedTime = new Date(year, month, day, hour, minute, sec)
			WpLastUpdatedTime.setTime(WpLastUpdatedTime.getTime() + SWEREW_QE_TimeDiff * 3600000)

			var LastLookedTime = new Date(2000, 1, 1, 0, 0);
			var UTxt = "NEW!";
			if (jsCluObj.hasOwnProperty(wpguid)) {
				//LastLookedTime = new Date(jsCluObj[wpguid.date]);
                LastLookedTime = new Date(jsCluObj[wpguid]['date']);
				if (LastLookedTime < WpLastUpdatedTime) {
					UTxt = "UPDATED!"
				} else {
					UTxt = ""
				}
			}

			// Check if Last looked date is earlier than updated or looked date for held caches
			// is older than 3 days and mark if so.
			if (CacheIsHeld == true) {
				if (LastLookedTime < WpLastUpdatedTime || WpLastUpdatedTime < new Date() - 259200000) {
					console.log(wpguid,LastLookedTime,WpLastUpdatedTime);
					// Yes. Mark cache as updated or old

					// Check to se if cache is more than x (three) days old since last update.
					if (Ss) {
						if (WpLastUpdatedTime < new Date() - 259200000) {
							// Yes, old. Change warning text
							UTxt = "OLDER THAN 3 DAYS!";
						}
					}

					if (Ss) {
						// Show status
						if(UTxt != '') {
							var tRow = document.getElementById('cb_' + wptid).parentNode;
							if (tRow) {
								var tCell = tRow.cells[5];
								var CluSpan = document.createElement('span');
								CluSpan.id = 'Clu_' + wptid;
								//if (UTxt == 'LOCKED!') {CluSpan.classList.add(ColorSel_OtYd)};
								if (UTxt == 'OLDER THAN 3 DAYS!') {CluSpan.classList.add(ColorSel_OtXd)};
								if (UTxt == 'NEW!') {CluSpan.classList.add(ColorSel_N)};
								if (UTxt == 'UPDATED!') {CluSpan.classList.add(ColorSel_U)};
									CluSpan.appendChild(document.createTextNode(UTxt));
									tCell.appendChild(CluSpan);
							}
						}
					}
				}

				// Check if cache is on watch
				if (wpwatch == "true") {
					UTxt = "";
				} else {
					UTxt = "NO WATCH";
				}

				if (Sws) {
					// Show watchstatus
					if(UTxt != '') {
						tRow = document.getElementById('cb_' + wptid).parentNode;
						if (tRow) {
							tCell = tRow.cells[5];
							CluSpan = document.createElement('span');
							CluSpan.id = 'Clu_' + wptid;
							CluSpan.classList.add(ColorSel_Sws);
							CluSpan.appendChild(document.createTextNode(UTxt));
							tCell.appendChild(CluSpan);
						}
					}
				}
			} else {
				// Check if Last looked date is earlier than updated or looked date for nonheld caches
				// is older than 7 days and mark if so.
				if (LastLookedTime < WpLastUpdatedTime || WpLastUpdatedTime < new Date() - 604800000 ) {
					// Yes. Mark cache as updated or old

					if (Ss) {
						// Check to se if cache is more than tree days old since last update.
						if (WpLastUpdatedTime < new Date() - 604800000) {
							// Yes, old. Change warning text
							UTxt = "OLDER THAN 7 DAYS!";
						}
					}

					// Show status
					if (Ss) {
						if(UTxt != '') {
							tRow = document.getElementById('cb_' + wptid).parentNode;
							if (tRow) {
								tCell = tRow.cells[5];
								CluSpan = document.createElement('span');
								CluSpan.id = 'Clu_' + wptid;
								//if (UTxt == 'LOCKED!') {CluSpan.classList.add(ColorSel_OtYd)};
								if (UTxt == 'OLDER THAN 7 DAYS!') {CluSpan.classList.add(ColorSel_OtYd)};
								if (UTxt == 'NEW!') {CluSpan.classList.add(ColorSel_N)};
								if (UTxt == 'UPDATED!') {CluSpan.classList.add(ColorSel_U)};
								CluSpan.classList.add(ColorSel_OtYd);
								CluSpan.appendChild(document.createTextNode(UTxt));
								tCell.appendChild(CluSpan);
							}
						}
					}
				}
				// Check if cache is on watch
				if (wpwatch == "true") {
					UTxt = "WATCH";
				} else {
					UTxt = "";
				}

				// Show watchstatus
				if (Sws) {
					if(UTxt != '') {
						tRow = document.getElementById('cb_' + wptid).parentNode;
						if (tRow) {
							tCell = tRow.cells[5];
							CluSpan = document.createElement('span');
							CluSpan.id = 'Clu_' + wptid;
							CluSpan.classList.add(ColorSel_Sws);
							CluSpan.appendChild(document.createTextNode(UTxt));
							tCell.appendChild(CluSpan);
						}
					}
				}
			}
		}
		// Delete database.
		delete jsCluObj;
	}

// End general functions

// Settings menu

// Show Edit Settings box.
	function fEditSettings() {

		// If div already exists, reposition browser, and show alert.
		var divSet = document.getElementById("gm_divSet");
		if (divSet) {
			var YOffsetVal = parseInt(divSet.getAttribute('YOffsetVal', '0'));
			window.scrollTo(window.pageXOffset, YOffsetVal);
			alert('Edit Setting interface already on screen.');
			return;
		}


		// Create div.
		divSet = document.createElement('div');
		divSet.id = 'gm_divSet';
		divSet.setAttribute('style', 'position: absolute; ' +
				'padding: 5px; outline: 6px ridge blue; background: #FFFFCC;');
		var popwidth = parseInt(window.innerWidth * .5);
		divSet.style.width = popwidth + 'px';

		// Create heading.
		var ds_Heading = document.createElement('span');
		ds_Heading.style.fontSize = 'medium';
		ds_Heading.style.fontStyle = 'italic';
		ds_Heading.style.fontWeight = 'bold';
		ds_Heading.appendChild(document.createTextNode('"GCR SweReview v2 Queue Enhancer" script settings'));
		divSet.appendChild(ds_Heading);

		// Create duration selector.
		var ds_TimeP = document.createElement('p');
			ds_TimeP.style.textAlign = 'left';
			ds_TimeP.style.marginLeft = '6px';
			ds_TimeP.appendChild(document.createTextNode("Time difference Seattle - Sweden: "));
			var ds_TimeSel = document.createElement("select");
				ds_TimeSel.id = 'ds_TimeSel';
				for (var i = 8; i <= 10; i++) {
					var ds_TimeOpt = document.createElement("option");
						ds_TimeOpt.value = i;
						ds_TimeOpt.id = 'ds_TimeOpt' + i;
						ds_TimeOpt.text = i.toString();
						if (i == SWEREW_QE_TimeDiff) { ds_TimeOpt.selected = true; }
						ds_TimeSel.appendChild(ds_TimeOpt);
				}
			ds_TimeP.appendChild(ds_TimeSel);
			ds_TimeP.appendChild(document.createTextNode(" hours."));
		divSet.appendChild(ds_TimeP);

		// Show "Watch status"
		var pSws = document.createElement('p');
			pSws.style.textAlign = 'left';
			pSws.style.marginLeft = '6px';
			var cbSws = document.createElement('input');
				cbSws.id = 'cbSws';
				cbSws.type = 'checkbox';
				cbSws.style.marginRight = '6px';
				var Sws = GM_getValue('Sws' , false);
				cbSws.checked = (Sws == true);
		pSws.appendChild(cbSws);
		pSws.appendChild(document.createTextNode("Show watch status? - Color:"));
		divSet.appendChild(pSws);
			// Create color selector.
			var ds_ColorL = document.createElement('l');
				ds_ColorL.style.textAlign = 'left';
				ds_ColorL.style.marginLeft = '6px';
				var ds_ColorSel_pSws = document.createElement("select");
					ds_ColorSel_pSws.id = 'ds_ColorSel_Sws';
				for (i = 0; i <= 2; i++) {
					ds_ColorOpt = document.createElement("option");
					ds_ColorOpt.value = colorvalue[i];
					ds_ColorOpt.id = 'ds_ColorOpt' + i;
					ds_ColorOpt.text = color[i];
					if (colorvalue[i] == ColorSel_Sws) { ds_ColorOpt.selected = true; }
					ds_ColorSel_pSws.appendChild(ds_ColorOpt);
				}
				ds_ColorL.appendChild(ds_ColorSel_pSws);
			pSws.appendChild(ds_ColorL);

		// Show "Status"
		var pSs = document.createElement('p');
			pSs.style.textAlign = 'left';
			pSs.style.marginLeft = '6px';
				var cbSs = document.createElement('input');
					cbSs.id = 'cbSs';
					cbSs.type = 'checkbox';
					cbSs.style.marginRight = '6px';
					var Ss = GM_getValue('Ss' , false);
					cbSs.checked = (Ss == true);
		pSs.appendChild(cbSs);
		pSs.appendChild(document.createTextNode("Show status?"));
		divSet.appendChild(pSs);

		// Show "New"
		var pN = document.createElement('p');
			pN.style.textAlign = 'left';
			pN.style.marginLeft = '97px';
		pN.appendChild(document.createTextNode("* New - Color:"));
		divSet.appendChild(pN);
			// Create color selector.
			ds_ColorL = document.createElement('l');
			ds_ColorL.style.textAlign = 'left';
			ds_ColorL.style.marginLeft = '6px';
				var ds_ColorSel_pN = document.createElement("select");
				ds_ColorSel_pN.id = 'ds_ColorSel_N';
				for (i = 0; i <= 2; i++) {
					ds_ColorOpt = document.createElement("option");
					ds_ColorOpt.value = colorvalue[i];
					ds_ColorOpt.id = 'ds_ColorOpt' + i;
					ds_ColorOpt.text = color[i];
					if (colorvalue[i] == ColorSel_N) { ds_ColorOpt.selected = true; }
					ds_ColorSel_pN.appendChild(ds_ColorOpt);
				}
			ds_ColorL.appendChild(ds_ColorSel_pN);
		pN.appendChild(ds_ColorL);


		// Show "Updated"
		var pU = document.createElement('p');
			pU.style.textAlign = 'left';
			pU.style.marginLeft = '70px';
		pU.appendChild(document.createTextNode("* Updated - Color:"));
		divSet.appendChild(pU);
			// Create color selector.
			ds_ColorL = document.createElement('l');
			ds_ColorL.style.textAlign = 'left';
			ds_ColorL.style.marginLeft = '6px';
				var ds_ColorSel_pU = document.createElement("select");
				ds_ColorSel_pU.id = 'ds_ColorSel_U';
				for (i = 0; i <= 2; i++) {
					ds_ColorOpt = document.createElement("option");
					ds_ColorOpt.value = colorvalue[i];
					ds_ColorOpt.id = 'ds_ColorOpt' + i;
					ds_ColorOpt.text = color[i];
					if (colorvalue[i] == ColorSel_U) { ds_ColorOpt.selected = true; }
					ds_ColorSel_pU.appendChild(ds_ColorOpt);
				}
			ds_ColorL.appendChild(ds_ColorSel_pU);
		pU.appendChild(ds_ColorL);

		// Show "Older than X days"
		var pOtXd = document.createElement('p');
			pOtXd.style.textAlign = 'left';
			pOtXd.style.marginLeft = '6px';
		pOtXd.appendChild(document.createTextNode("* Older than 3 days - Color:"));
		divSet.appendChild(pOtXd);
			// Create color selector.
			ds_ColorL = document.createElement('l');
			ds_ColorL.style.textAlign = 'left';
			ds_ColorL.style.marginLeft = '6px';
				var ds_ColorSel_pOtXd = document.createElement("select");
				ds_ColorSel_pOtXd.id = 'ds_ColorSel_OtXd';
				for (i = 0; i <= 2; i++) {
					ds_ColorOpt = document.createElement("option");
					ds_ColorOpt.value = colorvalue[i];
					ds_ColorOpt.id = 'ds_ColorOpt' + i;
					ds_ColorOpt.text = color[i];
					if (colorvalue[i] == ColorSel_OtXd) { ds_ColorOpt.selected = true; }
					ds_ColorSel_pOtXd.appendChild(ds_ColorOpt);
				}
			ds_ColorL.appendChild(ds_ColorSel_pOtXd);
		pOtXd.appendChild(ds_ColorL);

		// Show "Older than Y days"
		var pOtYd = document.createElement('p');
			pOtYd.style.textAlign = 'left';
			pOtYd.style.marginLeft = '6px';
		pOtYd.appendChild(document.createTextNode("* Older than 7 days - Color:"));
		divSet.appendChild(pOtYd);
			// Create color selector.
			ds_ColorL = document.createElement('l');
			ds_ColorL.style.textAlign = 'left';
			ds_ColorL.style.marginLeft = '6px';
				var ds_ColorSel_pOtYd = document.createElement("select");
				ds_ColorSel_pOtYd.id = 'ds_ColorSel_OtYd';
				for (i = 0; i <= 2; i++) {
					ds_ColorOpt = document.createElement("option");
					ds_ColorOpt.value = colorvalue[i];
					ds_ColorOpt.id = 'ds_ColorOpt' + i;
					ds_ColorOpt.text = color[i];
					if (colorvalue[i] == ColorSel_OtYd) { ds_ColorOpt.selected = true; }
					ds_ColorSel_pOtYd.appendChild(ds_ColorOpt);
				}
			ds_ColorL.appendChild(ds_ColorSel_pOtYd);
		pOtYd.appendChild(ds_ColorL);

		// Create buttons.
		divSet.appendChild(document.createElement('p'));
		var ds_ButtonsP = document.createElement('p');
		ds_ButtonsP.style.textAlign = 'right';
		var ds_SaveButton = document.createElement('button');
		ds_SaveButton = document.createElement('button');
		ds_SaveButton.appendChild(document.createTextNode("Save"));
		ds_SaveButton.addEventListener("click", fSaveButtonClicked, true);
		var ds_CancelButton = document.createElement('button');
		ds_CancelButton.style.marginLeft = '6px';
		ds_CancelButton.addEventListener("click", fCancelButtonClicked, true);
		ds_CancelButton.appendChild(document.createTextNode("Cancel"));
		ds_ButtonsP.appendChild(ds_SaveButton);
		ds_ButtonsP.appendChild(ds_CancelButton);
		divSet.appendChild(ds_ButtonsP);

		// Add div to page.
		var toppos = parseInt(window.pageYOffset + 100);
		var leftpos = parseInt((window.innerWidth / 2) - (popwidth / 2));
		divSet.style.top = toppos + 'px';
		divSet.style.left = leftpos + 'px';
		divSet.setAttribute('YOffsetVal', window.pageYOffset);
		document.body.appendChild(divSet);

	// Save values.
	function fSaveButtonClicked() {

		// Save Timediff.
		SWEREW_QE_TimeDiff = ds_TimeSel.options[ds_TimeSel.selectedIndex].value;
		GM_setValue("SWEREW_QE_TimeDiff", SWEREW_QE_TimeDiff);

		// Save Show watchstatus.
		var cbSws = document.getElementById('cbSws');
		GM_setValue('Sws' , (cbSws.checked)?true:false);
		var ColorSel_Sws = ds_ColorSel_pSws.options[ds_ColorSel_pSws.selectedIndex].value;
		GM_setValue("ColorSel_Sws", ColorSel_Sws);

		// Save Show status.
		var cbSs = document.getElementById('cbSs');
		GM_setValue('Ss' , (cbSs.checked)?true:false);

		// Save New color
		var ColorSel_N = ds_ColorSel_pN.options[ds_ColorSel_pN.selectedIndex].value;
		GM_setValue("ColorSel_N", ColorSel_N);

		// Save Updated color
		var ColorSel_U = ds_ColorSel_pU.options[ds_ColorSel_pU.selectedIndex].value;
		GM_setValue("ColorSel_U", ColorSel_U);

		// Save Show older than X days color.
		ColorSel_OtXd = ds_ColorSel_pOtXd.options[ds_ColorSel_pOtXd.selectedIndex].value;
		GM_setValue("ColorSel_OtXd", ColorSel_OtXd);

		// Save Show older than Y days color.
		ColorSel_OtYd = ds_ColorSel_pOtYd.options[ds_ColorSel_pOtYd.selectedIndex].value;
		GM_setValue("ColorSel_OtYd", ColorSel_OtYd);

		alert('Settings have been saved.');
		location.reload();
		fCloseSettingsDiv();
	}

	// Cancel request.
	function fCancelButtonClicked() {
		var resp = confirm('Any changes will be lost. Close Settings?');
		if (resp) {
			fCloseSettingsDiv();
		}
	}

	// Remove element and everything below it.
	function removeNode(element) {
		element.parentNode.removeChild(element);
	}

	// Close settings window
	function fCloseSettingsDiv() {
		var divBlackout = document.getElementById('divBlackout');
		var divSet = document.getElementById('gm_divSet');
		var winTopPos = divSet.getAttribute('winTopPos') - 0; // snapback code
		removeNode(divSet);
		removeNode(divBlackout);
		window.removeEventListener('resize', fSetLeftPos, true);
		document.documentElement.scrollTop = winTopPos; // snapback code
	}

	function fSetLeftPos() {
		var divSet = document.getElementById('gm_divSet');
			if (divSet) {
				var popwidth = parseInt(window.innerWidth * .5);
				divSet.style.width = popwidth + 'px';
				var leftpos = parseInt((window.innerWidth / 2) - (popwidth / 2));
				divSet.style.left = leftpos + 'px';
			}
		}
	}
// End settings menu