/*
Geocaching Publish with Timestamp - v01.00 2013-03-24
(c) 2013, Toa Norik

Greasemonkey user script: see http://greasemonkey.mozdev.org

Changes log defaults, based on log type. Auto-submits handicap notes.

Log type codes:
	 4 = Write Note
    24 = Publish
	18 = Reviewer Note (unpublished)
	68 = Reviewer Note (published)
   Tps = Publish with Time stamp

Change Log:

* (v01.00) Initial Version


// ==UserScript==
// @name           GCR SweRevLog
// @description    Additions for Swedenreviewers 
// @version        01.00
// @namespace      http://www.dahlin.cx/
// @include        http://*.geocaching.com/seek/log.aspx*
// @icon           http://www.dahlin.cx/geocaching/swerev/greasemonkey/GCR_SweRevLog/swefrog.png
// ==/UserScript==

*/

	//  Check session storage to see if auto-close was requested.
	if (sessionStorage.getItem('autoclose') == 'true') {
		window.close();
		return;
	}
	//  If auto-close requested, set session storage value.
	if (UrlParm('close') == 'y') {
		//  Set session storage value to close tab on redisplay.
		sessionStorage.setItem('autoclose', 'true');
	}

	// Set general auto submit value.
	if (UrlParm('auto') == 'y') {
		var AutoSubmit = true;
	}


	// Locate textarea element.
	var e_LogBookPanel1_tbLogInfo = document.getElementById("ctl00_ContentBody_LogBookPanel1_uxLogInfo");
	if (! e_LogBookPanel1_tbLogInfo) {
		var txtAreas = document.getElementsByTagName('textarea');
		if (txtAreas.length == 1) {
			e_LogBookPanel1_tbLogInfo = txtAreas[0];
		}
	}


	// Only apply following changes if signed on as a reviewer.
	var e_chkCannotDelete = document.getElementById("ctl00_ContentBody_LogBookPanel1_chkCannotDelete");
	if (e_chkCannotDelete != null) {

		var e_LogBookPanel1_LogButton = document.getElementById("ctl00_ContentBody_LogBookPanel1_LogButton");

		//  Add event listener to log type dropdown.
		e_LogType = document.getElementById("ctl00_ContentBody_LogBookPanel1_ddLogType");
		e_LogType.addEventListener("change", TypeChange, true);


		//  Get LogType.
		var LogType = UrlParm('LogType');

//******************************* SPARAS *************************************
		//  If Time Publish Stamp.
		if (LogType == 'tps') {
            var time=new Date()
            var time=time.getHours()+':'+time.getMinutes()
            e_LogType.value = '24'
            e_LogBookPanel1_tbLogInfo.value='Published at ' + time;
            e_chkCannotDelete.checked=true;
		}
	}

	if (AutoSubmit) {
		fSubmitLog();
	}

// ---------------------------------- Functions ---------------------------------- //

	//  Autosubmit a log, if requested.
	function fSubmitLog() {
	//  Function to click the log submit button.
		var e_LogBookPanel1_LogButton =
				document.getElementById("ctl00_ContentBody_LogBookPanel1_LogButton");
		e_LogBookPanel1_LogButton.disabled = false;
		var ClickLogSubmitButton = function() {
			if (e_LogBookPanel1_LogButton) {
				e_LogBookPanel1_LogButton.click();
			}
		}
		TimeOutID = window.setTimeout(ClickLogSubmitButton, 250);
	}


//************************** FUNCTIONS *************************
	//  Set the Always Cannot Delete option on/off.
	function fOptRevNoteUndel() {
		var OptRevNoteUndel = GM_getValue('OptRevNoteUndel', 'Off');
		var Resp = confirm(
				'Click OK to always automatically set the Cannot Delete\n' +
				'option for a log.\n\n' +
				'Click CANCEL to turn this option Off.\n\n' +
				'This option is currently turned ' + OptRevNoteUndel + '.');
		if (Resp) {
			OptRevNoteUndel = 'On';
		} else {
			OptRevNoteUndel = 'Off';
		}
		GM_setValue('OptRevNoteUndel', OptRevNoteUndel);
		alert('Auto Cannot Delete has been turned ' + OptRevNoteUndel + '.');
	}

	//  Process log type change.
	function TypeChange() {
		newType = this.value - 0;
		switch(newType) {
			case 24:
				e_LogBookPanel1_tbLogInfo.value='Published';
				if (e_chkCannotDelete) { e_chkCannotDelete.checked = true; }
				e_LogBookPanel1_tbLogInfo.focus();
				e_LogBookPanel1_LogButton.focus();
				break;
			case 25:
				e_LogBookPanel1_tbLogInfo.value='Listing retracted.';
				if (e_chkCannotDelete) { e_chkCannotDelete.checked = true; }
				e_LogBookPanel1_tbLogInfo.focus();
				e_LogBookPanel1_LogButton.focus();
				break;
			case 18: case 68:
				if (GM_getValue('OptRevNoteUndel', 'Off') == 'On') {
					if (e_chkCannotDelete) { e_chkCannotDelete.checked = true; }
				}
				e_LogBookPanel1_tbLogInfo.focus();
				e_LogBookPanel1_LogButton.focus();
				break;
		}
	}


	//  Returns a URL parameter.
	//    ParmName - Parameter name to look for.
	//    IgnoreCase - (optional) *false, true. Ignore parmeter name case.
	//    UrlString - (optional) String to search. If omitted, document URL is used.
	function UrlParm(ParmName, IgnoreCase, UrlString) {
		var RegRslt, sc = '', RtnVal = '';
		if (IgnoreCase) {sc = 'i'}
		if(UrlString) {
			var PageUrl = UrlString;
		} else {
			PageUrl = document.location + '';
		}
		var ParmString = PageUrl.substring(PageUrl.indexOf('?') + 1);
		var RegEx1 = new RegExp('(^|&)' + ParmName + '=(.*?)(&|#|$)', sc);
		RegRslt = RegEx1.exec(ParmString);
		if (RegRslt) {RtnVal = RegRslt[2]}
		return RtnVal;
	}
