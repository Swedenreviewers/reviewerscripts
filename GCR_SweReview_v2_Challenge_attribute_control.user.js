/*
* v01.11	Clean up code

* v01.10    Fixed update link

* v01.00    New Version of SweRev-Script. Version 2

// ==UserScript==
// @name			    GCR SweReview v2 Challenge attribute control
// @description		    Check for challenge attribute
// @namespace		    https://admin.geocaching.com
// @author			    Sweden Reviewers Toa Norik
// @version			    01.11
// @icon			    https://i.imgur.com/zA6Y7hV.png
// @updateURL		    https://gitlab.com/Swedenreviewers/reviewerscripts/raw/master/GCR_SweReview_v2_Challenge_attribute_control.user.js
// @downloadURL		    https://gitlab.com/Swedenreviewers/reviewerscripts/raw/master/GCR_SweReview_v2_Challenge_attribute_control.user.js
// @include			    http*://*.geocaching.com/admin/review.aspx*
// ==/UserScript==
*/

	// Images
	var GeneralWarningImg =
		"data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAABgAAAAYCAYAAADgdz34AAAE" +
		"EElEQVR42s2Va2wUVRTH%2F3dmOn1tt8u23b63BW0MQmIgQVJEVOSRSHdJSkRitFb5QqLx" +
		"izF8UuojxugXY2JQE8MXXx%2BA2lbUNhIxKG0CJK3YYEu3S9dut%2Fvqe7vdeV3PTMmSZc" +
		"VW1MRJbu7dc885v7nnP%2Fcsw3%2F8sP8VYKip%2BagOXnvv1%2B0v%2FOuAod1eXrRxLT" +
		"gTEPvVh%2Ft6OlcVuyqna3u8vMLzAGRRAY9FkVIYfBcD2NzdvmL8ig7XvQePiY6iNte2dV" +
		"BHRihChCSLCIzMIrGotG3qOv3aPwL4HmniNUe8SP3YA1ZSSRYOHpmEZHfiyi8RbO3%2Bit" +
		"0xILDvQKRwy%2FqyPGEKOQ%2FtR86OPZZdOdcD7dQJTCVyEAovhO%2F%2Fpr3izgB7m3lF" +
		"6y6kzn0H24cdGXtzLR7kllfhylAcW7pOsb8NCO7Zz%2B3e7ZASIWh%2BP2zHT2cCnn4MUr" +
		"kbc7MJ%2BIPz2PZtB1s1YNJ78HVWUvSK89ENUHx%2BGKHxbECrF5KrFjJpMuifRlLV3mjs" +
		"OPnqqgChnU3c%2FtRuWoxaX40RmoDt%2FS8yfGYe3gihqh6CqkKXCnAppmLX92fYioCw9%" +
		"2FFOuaHGk7%2FJDT4ehB6PQbvci%2BKzv2X4xdcWQKy9G1B1yDpH%2F%2FUYwkne1bwQ8%" +
		"2F4lIHKghTsONSLZ3QFjegqCKMMYHYHjYigT0GBPA5gGyAZD1%2BA4nkjE2W0Bkzv2TuQ2" +
		"lFfKYhjaYmp52%2BAwfMNw9AVvC%2BAqh6QDv8cXMBCdDTy5MFWXBQi76jzaktrpfKcF6s" +
		"994JJkBVsA%2FygcF8YyAeudECvr0wCmGigivU4OB82s3kNz8a4MQCivlNvbnoXu66cguq" +
		"2mUbsBmJlGzs4mFL70puW78PZRpL48AdFZS76aBQABRCpTMqHgzHgEh5MzLA2IuuqOweVs" +
		"K3imEfrVYbII4IaxDNBpVlTqQ1dhTE4vH9teSKMYor0iA8A1jiLOcHYihmhy6aOWxdkjFm" +
		"BScHD7Jy9DO%2F8DIEpW0jTAPEEkBNt7n0PasNkCaAOXMPdcM4Ti0nSJTAAIINBsiv5pII" +
		"jnU%2FOMRddU98meB7cKbomaWJx6Gc8G%2BK6RyOOZGtzjgFi9LgtgzrkUNjgzj%2F75uS" +
		"iL5pVx2%2FEXoVw4T3dKpuRaNmCMRP7pVpFLSWT3nwLMkU%2Bl%2BmxiAixaVs9t77ZC6e" +
		"0lQczy6FkAJBIAtefCtz6AUFJBjW4ftNEhiI6qtAb8FoCTcn0cDBDAUT2W59nuZncJwHzy" +
		"JkAzLxANWjMq29LAIAUq5t8BWH4%2BaSUi11EOpJZPICg3ATIV4XJ8xizR8lcUXlMzRjnc" +
		"cNlg0LU36O11xojDodMGpbB%2Bm4fRuW7NKtkNgms39jVaW%2F4wEF1SyG4EDi%2FO1v0B" +
		"daNJ3oB8YjwAAAAASUVORK5CYII%3D";


var PathSearch = "//a[contains(@href, 'project-gc.com/Challenges/')]";
var ccLink = document.evaluate(PathSearch, document, null,
                               XPathResult.FIRST_ORDERED_NODE_TYPE, null).singleNodeValue;
if (ccLink) {

// Check if Challenge attribute is lit.
	var C_Icon = (document.body.innerHTML.search(/challengecache-yes.\png/g) >= 0);
		if (C_Icon)	{
			return;
		}
    fShowAlert('No attribute for challenges');
} else {
    // Check if Challenge attribute is lit.
	C_Icon = (document.body.innerHTML.search(/challengecache-yes.\png/g) >= 0);
		if (!C_Icon)	{
			return;
		}

    fShowAlert('Attribute for challenges set but no challenge');
}

// General functions

//Function for getting elements by ID
function $() {
	return document.getElementById(arguments[0]);
}

//Insert element ahead of an existing element.
function insertAheadOf(newElement, anchorElement) {
	anchorElement.parentNode.insertBefore(newElement, anchorElement);
}

//Show alert warning.
function fShowAlert(alertText) {
	var cdl = document.getElementsByClassName('CacheDetailsList')[0];
	var divNFP = $('divNFP');
	if (!divNFP) {
		divNFP = document.createElement('div');
		divNFP.id = 'divNFP';
		insertAheadOf(divNFP, cdl);
		divNFP.classList.add('nfw-warn');
		divNFP.classList.add('dropshadow_se');
	} else {
		divNFP.appendChild(document.createElement('br'));
	}
	var imgNFP = document.createElement('img');
	imgNFP.src = GeneralWarningImg;
	imgNFP.style.marginRight = '12px'
	divNFP.appendChild(imgNFP);
	divNFP.appendChild(document.createTextNode(alertText));
}

// End general functions