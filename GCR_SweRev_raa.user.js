﻿/*
* v01.10 Add version checker

* v01.00 Add link to Riksantikvarieambetet.
	
// ==UserScript==
// @name           GC SweRevRaa
// @description    Adds link to Riksantikvarieambetet
// @version        01.10
// @namespace      http://www.dahlin.cx/
// @include        http://www.geocaching.com/seek/cache_details.aspx*
// @require        SweRevVerChk.js
// @icon           http://www.dahlin.cx/geocaching/swerev/greasemonkey/GCR_SweRev/swefrog.png
// @copyright      Sweden Reviewers - Toa Norik
// ==/UserScript==
*/

// ------------------------Version Checking------------------------ //
fCheckScriptVersion('118aca5b-960e-4649-9449-efa8d8c9af31', '01.10');
// ------------------------Version Checking------------------------ //


//

// Get coordinates for this cache.
                    var e_CacheDetails_Coords = document.getElementById("uxLatLon");
                    var txCoords = e_CacheDetails_Coords.firstChild;
                        while (txCoords.nodeName != '#text') {
                            txCoords = txCoords.firstChild;
                        }
                    txCoords = txCoords.data;

//Insert RAA-link
                    var e_raa = document.getElementById('ctl00_ContentBody_LocationSubPanel');
                    e_raa.parentNode.appendChild(document.createTextNode(' | '));
                    var RaaDiv = document.createElement('a');
                    RaaDiv.id = 'RaaDiv';                   
                    RaaDiv.appendChild(document.createTextNode('Riksantikvarie\u00e4mbetet'));                   
                    RaaDiv.name = 'Raalink';
					RaaDiv.target = '_blank';
					RaaDiv.title = 'Link to Riksantikvarieambetet';
					//RaaDiv.style.color = 'green';
					RaaDiv.style.fontSize = 'xx-small';
					//RaaDiv.style.fontWeight = 'bold';
					RaaDiv.href = 'http://www.swedenreviewers.se/raa/konv1.asp?latlon=' + encodeURIComponent(txCoords)  + '&Zoom=500&go=go';
                    e_raa.parentNode.appendChild(RaaDiv);
                    e_raa.parentNode.appendChild(document.createTextNode(' | '))
